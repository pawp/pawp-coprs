# (cg) Cheater...
%define Werror_cflags %nil

# (aco) Needed for the dri drivers
%define _disable_ld_no_undefined 1

# https://cgit.freedesktop.org/mesa/mesa/commit/?h=13.0&id=3bb0415ab96f74183f7aa58c1a543448653ccb3e
%define git 1
%define git_branch 19.3

%define opengl_ver 4.5

# define version, RC & release
%define main_version		19.2.0
%define relc			3
%define rel			1

%define release			%mkrel %{?relc:0.rc%{relc}.}%{rel}

%if %{git}
%if %{?relc:1}%{!?relc:0}
%define release			%mkrel 1~rc%{relc}.1.git%{git}.%{rel}
%else
%define release			1.git%{git}.%{rel}
%endif
%endif

%define makedepend		%{_bindir}/gccmakedep

%define clname			mesaopencl
%define d3dname			d3d
%define eglname			mesaegl
%define glname			mesagl
%define glwname			mesaglw
%define glesv1name		mesaglesv1
%define glesv2name		mesaglesv2
%define glapiname		glapi
%define gbmname			gbm
%define xaname			xatracker
%define vulkanname		mesavulkan

%define clmajor			1
%define d3dmajor		1
%define eglmajor		1
%define glmajor			1
%define glwmajor		1
%define glesv1major		1
%define glesv2major		2
%define glapimajor		0
%define gbmmajor		1
%define waylandeglmajor		1
%define osmesamajor		8
%define xamajor			2

%define libclname               %mklibname %{clname} %{clmajor}
%define libd3dname		%mklibname %{d3dname} %{d3dmajor}
%define libeglname              %mklibname %{eglname} %{eglmajor}
%define libglname		%mklibname %{glname} %{glmajor}
%define libglwname		%mklibname %{glwname} %{glwmajor}
%define libglesv1name		%mklibname %{glesv1name}_ %{glesv1major}
%define libglesv2name		%mklibname %{glesv2name}_ %{glesv2major}
%define libglapiname		%mklibname %{glapiname} %{glapimajor}
%define libgbmname		%mklibname %{gbmname} %{gbmmajor}
%define libwaylandeglname	%mklibname %{waylandeglname} %{waylandeglmajor}
%define libosmesaname		%mklibname osmesa %{osmesamajor}
%define libxaname		%mklibname %{xaname} %{xamajor}

%define dridrivers		%mklibname dri-drivers

%define vulkandrivers	%mklibname %{vulkanname}-drivers
%define vulkandevel		%mklibname %{vulkanname} -d

%define libcldevel		%mklibname mesaopencl -d
%define khrdevel		%mklibname mesakhr -d
%define osmesadevel		%mklibname osmesa -d

# Architecture-independent Virtual provides:
%define libd3dname_virt		lib%{d3dname}
%define libeglname_virt		lib%{eglname}
%define libglname_virt		lib%{glname}
%define libglwname_virt		lib%{glwname}
%define libglesv1name_virt	lib%{glesv1name}
%define libglesv2name_virt	lib%{glesv2name}
%define libglapiname_virt       lib%{glapiname}
%define libgbmname_virt       	lib%{gbmname}
%define libwaylandeglname_virt	lib%{waylandeglname}
%define libxaname_virt		lib%{xaname}

%define mesasrcdir		%{_prefix}/src/Mesa/

%define with_valgrind 1

%define with_vaapi 1

%global with_hardware 1
%global with_vdpau 1
%global with_vaapi 1
%global with_nine 1
%global with_omx 1
%global with_opencl 1
%global base_drivers nouveau,r100,r200

%ifarch %{ix86} x86_64
%global platform_drivers ,i915,i965
%global with_iris 1
%global with_vmware 1
%global with_xa     1
%global vulkan_drivers intel,amd
%else
%global vulkan_drivers amd
%endif

%ifarch %{arm} aarch64
%global with_etnaviv   1
%global with_freedreno 1
%global with_kmsro     1
%global with_tegra     1
%global with_vc4       1
%global with_xa        1
%endif

%ifnarch %{arm}
%global with_radeonsi 1
%endif

%global dri_drivers %{?base_drivers}%{?platform_drivers}

Name:		mesa
Version: 	19
Release: 	3
Summary:	OpenGL %{opengl_ver} compatible 3D graphics library
Group:		System/Libraries

# temp force new gcc
BuildRequires:	gcc >= 9.2.0-1

BuildRequires:	pkgconfig(xfixes)	>= 4.0.3
BuildRequires:	pkgconfig(xshmfence)
BuildRequires:	pkgconfig(xt)		>= 1.0.5
BuildRequires:	pkgconfig(xmu)		>= 1.0.3
BuildRequires:	pkgconfig(x11)		>= 1.3.3
BuildRequires:	pkgconfig(xdamage)	>= 1.1.1
BuildRequires:	pkgconfig(expat)	>= 2.0.1
BuildRequires:	gccmakedep
BuildRequires:	x11-proto-devel		>= 7.3
BuildRequires:	pkgconfig(libdrm)	>= 2.4.91
BuildRequires:	pkgconfig(xcb-dri3)
BuildRequires:	pkgconfig(xcb-present)  >= 1.11
BuildRequires:	pkgconfig(xext)		>= 1.1.1
BuildRequires:	pkgconfig(xxf86vm)	>= 1.1.0
BuildRequires:	pkgconfig(xi)		>= 1.3
BuildRequires:	pkgconfig(xrandr)
%if 0%{?with_omx}
BuildRequires:  pkgconfig(libomxil-bellagio)
%endif
%if 0%{?with_opencl}
BuildRequires:  clang-devel
BuildRequires:  pkgconfig(libclc)
%endif
BuildRequires:	pkgconfig(talloc)
BuildRequires:	python3-libxml2
BuildRequires:	python3-mako
BuildRequires:	makedepend
BuildRequires:	bison
BuildRequires:	flex
BuildRequires:	meson
BuildRequires:	pkgconfig(libelf)
BuildRequires:	llvm-devel >= 8.0
BuildRequires:	pkgconfig(libudev)
%if 0%{?with_valgrind}
BuildRequires:  pkgconfig(valgrind)
%endif
%if 0%{?with_vaapi}
BuildRequires:  pkgconfig(libva)
%endif
BuildRequires:  pkgconfig(vdpau)
BuildRequires: pkgconfig(wayland-client)
BuildRequires: pkgconfig(wayland-server)
BuildRequires: pkgconfig(wayland-protocols)
BuildRequires:	libgcrypt-devel
BuildRequires:	pkgconfig(wayland-protocols) >= 1.8
BuildRequires:	lm_sensors-devel

URL:		https://www.mesa3d.org
%if %{git}
#Source0:	%{name}-%{git}.tar.bz2
# (blino) snapshot of 13.0 branch
#Source0:	%{name}-%{git_branch}-%{git}.tar.xz
%else
Source0:	https://github.com/daniel-schuermann/mesa/archive/master.zip
%endif
Source3:	make-git-snapshot.sh

# mesa staging/19.2 branch from 1

# mesa master branch from 200

# mesa pending fixes from 300

#------------------------------------------------------------------------------

# package mesa
License:	MIT
Requires:	%{libglname} = 19-3
Recommends:	%{vulkandrivers} = 19-3
Provides:	hackMesa = %{version}
Obsoletes:	hackMesa <= %{version}
Provides:	Mesa = %{version}
Obsoletes:	Mesa < %{version}

%package -n	%{libglname}
Summary:	Files for Mesa (GL and GLX libs)
Group:		System/Libraries
Provides:	%{libglname_virt} = %{version}-%{release}
Requires:	%{dridrivers} >= %{version}-%{release}

%package -n	%{dridrivers}
Summary:	Mesa DRI drivers
Group:		System/Libraries
# do not require exact release to avoid conflicts when having
# x86_64 dri drivers from tainted and i586 dri drivers from core
Requires:	mesa = %{version}

%if 0%{?with_omx}
%package omx-drivers
Summary:        Mesa-based OMX drivers
%endif

%package -n %{libd3dname}
Summary:	Mesa Direct3D9 state tracker
Group:		System/Libraries
Provides:	%{libd3dname_virt} = %{version}-%{release}

%package -n %{libd3dname}-devel
Summary:	Development files for Mesa Direct3D9 state tracker
Group:		Development/C
Requires:	%{libd3dname} = %{version}-%{release}
Provides:	lib%{d3dname}-devel
Provides:	%{d3dname}-devel
Provides:	libd3d-devel

%package -n	%{libglname}-devel
Summary:	Development files for Mesa (OpenGL compatible 3D lib)
Group:		Development/C
Requires:	%{libglname} = %{version}-%{release}
Requires:	%{khrdevel} = %{version}-%{release}
Provides:	lib%{glname}-devel = %{version}-%{release}
Provides:	%{glname}-devel = %{version}-%{release}
Provides:	GL-devel
Provides:	libMesaGL-devel = %{version}-%{release}
Provides:	MesaGL-devel = %{version}-%{release}
Provides:	libgl-devel

%package -n	%{libeglname}
Summary:	Files for Mesa (EGL libs)
Group:		System/Libraries
Provides:	%{libeglname_virt} = %{version}-%{release}

%package -n	%{libeglname}-devel
Summary:	Development files for Mesa (EGL libs)
Group:		Development/C
Requires:	%{libeglname} = %{version}-%{release}
Requires:	%{khrdevel} = %{version}-%{release}
Provides:	EGL-devel
Provides:	lib%{eglname}-devel
Provides:	%{eglname}-devel
Provides:	libegl-devel

%package -n %{khrdevel}
Summary:	Mesa Khronos development headers
Group:		Development/C
Provides:	mesakhr-devel = %{version}-%{release}

%package -n %{libosmesaname}
Summary:	Mesa offscreen rendering library
Group:		System/Libraries

%package -n %{osmesadevel}
Summary:	Development files for libosmesa
Group:		Development/C
Requires:	%{libosmesaname} = %{version}-%{release}
Provides:	osmesa-devel = %{version}-%{release}

%package -n %{libglapiname}
Summary:	Files for mesa (glapi libs)
Group:		System/Libraries
Provides:	%{libglapiname_virt} = %{version}-%{release}

%package -n %{libglapiname}-devel
Summary:	Development files for glapi libs
Group:		Development/C
Requires:	%{libglapiname_virt} = %{version}-%{release}
Provides:	lib%{glapiname}-devel
Provides:	%{libglapiname}-devel

%if 0%{?with_opencl}
%package -n %libclname
Summary:        Mesa OpenCL runtime library
#Requires:       libclc%{?_isa}
#Requires:       %{name}-libgbm%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%package -n %libcldevel
Summary:        Mesa OpenCL development package
Requires:       %libclname%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
%endif
 

%package -n %{libglesv1name}
Summary:	Files for Mesa (glesv1 libs)
Group:		System/Libraries
Provides:	%{libglesv1name_virt} = %{version}-%{release}

%package -n %{libglesv1name}-devel
Summary:	Development files for glesv1 libs
Group:		Development/C
Requires:	%{libglesv1name} = %{version}-%{release}
Requires:	%{khrdevel} = %{version}-%{release}
Provides:	lib%{glesv1name}-devel
Provides:	%{glesv1name}-devel
Provides:	libglesv1-devel

%package -n %{libglesv2name}
Summary:	Files for Mesa (glesv2 libs)
Group:		System/Libraries
Provides:	%{libglesv2name_virt} = %{version}-%{release}

%package -n %{libglesv2name}-devel
Summary:	Development files for glesv2 libs
Group:		Development/C
Requires:	%{libglesv2name} = %{version}-%{release}
Requires:	%{khrdevel} = %{version}-%{release}
Provides:	lib%{glesv2name}-devel
Provides:	%{glesv2name}-devel
Provides:	libglesv2-devel

%package -n	%{libgbmname}
Summary:	Files for Mesa (gbm libs)
Group:		System/Libraries
Provides:	%{libgbmname_virt} = %{version}-%{release}

%package -n	%{libgbmname}-devel
Summary:	Development files for Mesa (gbm libs)
Group:		Development/C
Requires:	%{libgbmname} = %{version}-%{release}
Provides:	lib%{gbmname}-devel
Provides:	%{gbmname}-devel

%package -n	%{_lib}vdpau-driver-nouveau
Summary:	VDPAU plugin for nouveau driver
Group:		System/Libraries
# temporary:
%rename vdpau-driver-nouveau

%package -n	%{_lib}vdpau-driver-r300
Summary:	VDPAU plugin for r300 driver
Group:		System/Libraries
# temporary:
%rename vdpau-driver-r300

%package -n	%{_lib}vdpau-driver-r600
Summary:	VDPAU plugin for r600 driver
Group:		System/Libraries
# temporary:
%rename vdpau-driver-r600

%package -n	%{_lib}vdpau-driver-radeonsi
Summary:	VDPAU plugin for radeonsi driver
Group:		System/Libraries
# temporary:
%rename vdpau-driver-radeonsi

%package -n %{libxaname}
Summary:	Files for Mesa XA state tracker
Group:		System/Libraries
Provides:	%{libxaname_virt} = %{version}-%{release}

%package -n %{libxaname}-devel
Summary:	Development files for XA libs
Group:		Development/C
Requires:	%{libxaname_virt} = %{version}-%{release}
Provides:	lib%{xaname}-devel
Provides:	%{libxaname}-devel

%package -n %{vulkandrivers}
Summary:	Mesa Vulkan driver for Intel and Radeon GPUs
Group:		System/Libraries
%ifarch %{ix86} x86_64
Obsoletes:	%{_lib}vulkan_intel < 18.3.0-4
Provides:	%{_lib}vulkan_intel = %{version}-%{release}
%endif
Obsoletes:	%{_lib}vulkan_radeon < 18.3.0-4
Provides:	%{_lib}vulkan_radeon = %{version}-%{release}

%package -n %{vulkandevel}
Summary:	Mesa's Vulkan development files for Intel and Radeon drivers
Group:		Development/C
Requires:	%{vulkandrivers} = %{version}-%{release}
Obsoletes:	%{_lib}vulkan-devel < 0:18.3.0-4
Provides:   %{vulkanname}-devel = %{version}-%{release}

%package	common-devel
Summary:	Meta package for mesa devel
Group:		Development/C
Provides:	Mesa-common-devel = %{version}-%{release}
Provides:	hackMesa-common-devel = %{version}
Obsoletes:	Mesa-common-devel < %{version}
Obsoletes:	hackMesa-common-devel < %{version}
Requires:	%{libglname}-devel = %{version}
Requires:	mesaglu-devel
Requires:	freeglut-devel
Requires:	%{libeglname}-devel = %{version}
Requires:	%{libglesv1name}-devel = %{version}
Requires:	%{libglesv2name}-devel = %{version}

#------------------------------------------------------------------------------

%description
Mesa is an OpenGL %{opengl_ver} compatible 3D graphics library.

%description common-devel
Mesa common metapackage devel

%description -n	%{libeglname}
Mesa is an OpenGL %{opengl_ver} compatible 3D graphics library.
EGL parts.

%description -n	%{libeglname}-devel
Mesa is an OpenGL %{opengl_ver} compatible 3D graphics library.
EGL development parts.

%description -n	%{libglname}
Mesa is an OpenGL %{opengl_ver} compatible 3D graphics library.
GL and GLX parts.

%description -n %{dridrivers}
Mesa is an OpenGL %{opengl_ver} compatible 3D graphics library.
DRI drivers.

%if 0%{?with_omx}
%description omx-drivers
Mesa-based OMX drivers
%endif

%description -n	%{libglname}-devel
Mesa is an OpenGL %{opengl_ver} compatible 3D graphics library.

This package contains the headers needed to compile Mesa programs.

%description -n %{libglesv1name}
OpenGL ES is a low-level, lightweight API for advanced embedded graphics using
well-defined subset profiles of OpenGL.

This package provides the OpenGL ES library version 1.

%description -n %{khrdevel}
Mesa Khronos development headers.

%description -n %{libosmesaname}
Mesa offscreen rendering libraries for rendering OpenGL into
application-allocated blocks of memory.

%description -n %{osmesadevel}
This package contains the headers needed to compile programs against
the Mesa offscreen rendering library.

%description -n %{libglapiname}
This packages provides the glapi shared library used by gallium.

%description -n %{libglapiname}-devel
This package contains the headers needed to compile programes against glapi shared library.

%if 0%{?with_opencl}
%description -n %libclname
Mesa OpenCL runtime library

%description -n %libcldevel
Mesa OpenCL development package
%endif

%description -n %{libglesv1name}-devel
This package contains the headers needed to compile OpenGL ES 1 programs.

%description -n %{libd3dname}
Mesa Direct3D9 state tracker.

%description -n %{libd3dname}-devel
Mesa Direct3D9 state tracker development package.

%description -n %{libglesv2name}
OpenGL ES is a low-level, lightweight API for advanced embedded graphics using
well-defined subset profiles of OpenGL.

This package provides the OpenGL ES library version 2.

%description -n %{libglesv2name}-devel
This package contains the headers needed to compile OpenGL ES 2 programs.

%description -n	%{libgbmname}
Mesa is an OpenGL %{opengl_ver} compatible 3D graphics library.
GBM (Graphics Buffer Manager) parts.

%description -n	%{libgbmname}-devel
Mesa is an OpenGL %{opengl_ver} compatible 3D graphics library.
GBM (Graphics Buffer Manager) development parts.

%description -n %{libxaname}
This packages provides the xa shared library used by gallium.

%description -n %{libxaname}-devel
This package contains the headers needed to compile programes against xa shared library.

%description -n %{_lib}vdpau-driver-nouveau
This packages provides a VPDAU plugin to enable video acceleration
with the nouveau driver.

%description -n %{_lib}vdpau-driver-r300
This packages provides a VPDAU plugin to enable video acceleration
with the r300 driver.

%description -n %{_lib}vdpau-driver-r600
This packages provides a VPDAU plugin to enable video acceleration
with the r600 driver.

%description -n %{_lib}vdpau-driver-radeonsi
This packages provides a VPDAU plugin to enable video acceleration
with the radeonsi driver.

%description -n %{vulkandrivers}
This package contains the Vulkan parts for Mesa's Intel and Radeon drivers.

%description -n %{vulkandevel}
This package contains the development files for Mesa's Vulkan implementation.

#------------------------------------------------------------------------------

%prep
%autosetup -n %{name}-%{version}%{?relc:-rc%{relc}} -p1

%build
%meson -Dcpp_std=gnu++11 \
  -Dplatforms=x11,wayland,drm,surfaceless \
  -Ddri3=true \
  -Ddri-drivers=%{?dri_drivers} \
%if 0%{?with_hardware}
  -Dgallium-drivers=swrast,virgl,r300,nouveau%{?with_iris:,iris}%{?with_vmware:,svga}%{?with_radeonsi:,radeonsi,r600}%{?with_freedreno:,freedreno}%{?with_etnaviv:,etnaviv}%{?with_tegra:,tegra}%{?with_vc4:,vc4}%{?with_kmsro:,kmsro} \
%else
  -Dgallium-drivers=swrast,virgl \
%endif
  -Dgallium-vdpau=%{?with_vdpau:true}%{!?with_vdpau:false} \
  -Dgallium-xvmc=false \
  -Dgallium-omx=%{?with_omx:bellagio}%{!?with_omx:disabled} \
  -Dgallium-va=%{?with_vaapi:true}%{!?with_vaapi:false} \
  -Dgallium-xa=%{?with_xa:true}%{!?with_xa:false} \
  -Dgallium-nine=%{?with_nine:true}%{!?with_nine:false} \
  -Dgallium-opencl=%{?with_opencl:icd}%{!?with_opencl:disabled} \
  -Dvulkan-drivers=%{?vulkan_drivers} \
  -Dshared-glapi=true \
  -Dgles1=true \
  -Dgles2=true \
  -Dopengl=true \
  -Dgbm=true \
  -Dglx=dri \
  -Degl=true \
  -Dasm=%{?with_asm:true}%{!?with_asm:false} \
  -Dllvm=true \
  -Dshared-llvm=true \
  -Dvalgrind=%{?with_valgrind:true}%{!?with_valgrind:false} \
  -Dbuild-tests=false \
  -Dselinux=false \
  -Dosmesa=gallium \
  -Dgallium-extra-hud=true \
  -Dlmsensors=true \
  %{nil}
%meson_build

%install
%meson_install

# FIXME: strip will likely break the hardlink
# (blino) hardlink libGL files in %{_libdir}/mesa
# to prevent proprietary driver installers from removing them
mkdir -p $RPM_BUILD_ROOT%{_libdir}/mesa
pushd $RPM_BUILD_ROOT%{_libdir}/mesa
for l in ../libGL.so.*; do cp -a $l .; done
popd

%ifarch %{x86_64}
mkdir -p $RPM_BUILD_ROOT%{_prefix}/lib/dri
%endif

# libvdpau opens the versioned name, don't bother including the unversioned
rm -f %{buildroot}%{_libdir}/vdpau/*.so

# strip out useless headers
rm -f %{buildroot}%{_includedir}/GL/w*.h

# remove .la files
find %{buildroot} -name '*.la' -delete

%ifnarch %{ix86} x86_64
# To have something to package for mesavulkan-devel on arm/aarch64 for now
mkdir %{buildroot}%{_includedir}/vulkan
%endif

#------------------------------------------------------------------------------

%files
/usr/share/drirc.d/

%files -n %{dridrivers}
%dir %{_libdir}/dri
%{_libdir}/dri/*_dri.so
%{_libdir}/dri/*_drv*.so
%if 0%{?with_hardware}
%dir %{_libdir}/gallium-pipe
%{_libdir}/gallium-pipe/*.so
%endif

%if 0%{?with_omx}
%files omx-drivers
%{_libdir}/bellagio/libomx_mesa.so
%endif

%if 0%{?with_opencl}
%files -n %libclname
%{_libdir}/libMesaOpenCL.so.*
%{_sysconfdir}/OpenCL/vendors/mesa.icd

%files -n %libcldevel
%{_libdir}/libMesaOpenCL.so
%endif

%files -n %{libglname}
%{_libdir}/libGL.so.*
%dir %{_libdir}/mesa
%{_libdir}/mesa/libGL.so.%{glmajor}{,.*}

%files -n %{libeglname}
%{_libdir}/libEGL.so.%{eglmajor}{,.*}

%files -n %{libosmesaname}
%{_libdir}/libOSMesa.so.%{osmesamajor}{,.*}

%files -n %{libglapiname}
%{_libdir}/libglapi.so.%{glapimajor}{,.*}

%files -n %{libglesv1name}
%{_libdir}/libGLESv1_CM.so.%{glesv1major}{,.*}

%files -n %{libd3dname}
%{_libdir}/d3d/*.so.*

%files -n %{libglesv2name}
%{_libdir}/libGLESv2.so.%{glesv2major}{,.*}

%files -n %{libgbmname}
%{_libdir}/libgbm.so.%{gbmmajor}
%{_libdir}/libgbm.so.%{gbmmajor}.*

%files -n %{libxaname}
%{_libdir}/libxatracker.so.%{xamajor}{,.*}

%files -n %{libglname}-devel
%dir %{_includedir}/GL
%{_includedir}/GL/gl.h
%{_includedir}/GL/gl_mangle.h
%{_includedir}/GL/glext.h
%{_includedir}/GL/glx.h
%{_includedir}/GL/glx_mangle.h
%{_includedir}/GL/glxext.h
%{_includedir}/GL/glcorearb.h
%dir %{_includedir}/GL/internal
%{_includedir}/GL/internal/dri_interface.h
%{_libdir}/pkgconfig/dri.pc
%{_libdir}/libGL.so
%{_libdir}/pkgconfig/gl.pc

%files common-devel

%files -n %{libeglname}-devel
%{_includedir}/EGL
%{_libdir}/libEGL.so
%{_libdir}/pkgconfig/egl.pc

%files -n %{khrdevel}
%{_includedir}/KHR

%files -n %{osmesadevel}
%dir %{_includedir}/GL
%{_includedir}/GL/osmesa.h
%{_libdir}/libOSMesa.so
%{_libdir}/pkgconfig/osmesa.pc

%files -n %{libglapiname}-devel
%{_libdir}/libglapi.so

%files -n %{libglesv1name}-devel
%{_includedir}/GLES
%{_libdir}/libGLESv1_CM.so
%{_libdir}/pkgconfig/glesv1_cm.pc

%files -n %{libglesv2name}-devel
%{_includedir}/GLES2
%{_includedir}/GLES3
%{_libdir}/libGLESv2.so
%{_libdir}/pkgconfig/glesv2.pc

%files -n %{libd3dname}-devel
%{_includedir}/d3dadapter
%{_libdir}/d3d/*.so
%{_libdir}/pkgconfig/d3d.pc

%files -n %{libgbmname}-devel
%{_libdir}/libgbm.so
%{_includedir}/gbm.h
%{_libdir}/pkgconfig/gbm.pc

%files -n %{libxaname}-devel
%{_libdir}/libxatracker.so
%{_includedir}/xa_tracker.h
%{_includedir}/xa_composite.h
%{_includedir}/xa_context.h
%{_libdir}/pkgconfig/xatracker.pc

%files -n %{_lib}vdpau-driver-nouveau
%{_libdir}/vdpau/libvdpau_nouveau.so.*

%files -n %{_lib}vdpau-driver-r300
%{_libdir}/vdpau/libvdpau_r300.so.*

%if 0%{?with_radeonsi}
%files -n %{_lib}vdpau-driver-r600
%{_libdir}/vdpau/libvdpau_r600.so.*

%files -n %{_lib}vdpau-driver-radeonsi
%{_libdir}/vdpau/libvdpau_radeonsi.so.*
%endif

%files -n %{vulkandrivers}
%dir %{_datadir}/vulkan
%dir %{_datadir}/vulkan/icd.d
%ifarch %{ix86} x86_64
%{_datadir}/vulkan/icd.d/intel_icd*.json
%{_libdir}/libvulkan_intel.so
%endif
%{_datadir}/vulkan/icd.d/radeon_icd*.json
%{_libdir}/libvulkan_radeon.so

%files -n %{vulkandevel}
%dir %{_includedir}/vulkan
%ifarch %{ix86} x86_64
%{_includedir}/vulkan/vulkan_intel.h
%endif


%changelog
* Tue Sep 17 2019 tmb <tmb> 19.2.0-0.rc3.1.mga8
+ Revision: 1443134
- update to 19.2.0-rc3

* Tue Sep 17 2019 tmb <tmb> 19.1.7-1.mga8
+ Revision: 1443051
- update to 19.1.7

* Tue Sep 17 2019 tv <tv> 19.1.6-3.mga8
+ Revision: 1443040
- drop BR on python2-mako/libxml2
- rebuild for new llvm

* Sun Sep 08 2019 tmb <tmb> 19.1.6-2.mga8
+ Revision: 1438546
- add fixes from staging/19.1

* Tue Sep 03 2019 tmb <tmb> 19.1.6-1.mga8
+ Revision: 1436694
- update to 19.1.6
  * drop merged patches

* Sat Aug 31 2019 tmb <tmb> 19.1.5-3.mga8
+ Revision: 1436001
- add fixes from staging/19.1

* Sun Aug 25 2019 tmb <tmb> 19.1.5-2.mga8
+ Revision: 1431896
- radv: Change memory type order for GPUs without dedicated VRAM

* Fri Aug 23 2019 tmb <tmb> 19.1.5-1.mga8
+ Revision: 1431455
- update to 19.1.5

* Wed Aug 14 2019 tmb <tmb> 19.1.4-4.mga8
+ Revision: 1429310
- iris: Increase BATCH_SZ to 64kB
- i965/gen9: Optimize slice and subslice load balancing behavior

* Tue Aug 13 2019 tmb <tmb> 19.1.4-3.mga8
+ Revision: 1429140
- rebuild with gcc 9.2

* Sun Aug 11 2019 tmb <tmb> 19.1.4-2.mga8
+ Revision: 1428776
- add fixes from staging/19.1 branch

* Wed Aug 07 2019 tmb <tmb> 19.1.4-1.mga8
+ Revision: 1428079
- update to 19.1.4

* Fri Aug 02 2019 tmb <tmb> 19.1.3-4.mga8
+ Revision: 1426929
- add fixees from staging/19.1 branch

* Tue Jul 30 2019 tmb <tmb> 19.1.3-3.mga8
+ Revision: 1425974
- more fixes from staging/19.1

* Sun Jul 28 2019 tmb <tmb> 19.1.3-2.mga8
+ Revision: 1424805
- add post 19.1.3 fixes from staging/19.1

* Tue Jul 23 2019 tmb <tmb> 19.1.3-1.mga8
+ Revision: 1423610
- update to 19.1.3

* Tue Jul 09 2019 tmb <tmb> 19.1.2-1.mga8
+ Revision: 1419723
- update to 19.1.2

* Sat Jul 06 2019 tmb <tmb> 19.1.1-3.mga8
+ Revision: 1418993
- add fixes from staging/19.1

* Tue Jul 02 2019 tmb <tmb> 19.1.1-2.mga8
+ Revision: 1417532
- add fixes from staging/19.1

* Sat Jun 29 2019 tmb <tmb> 19.1.1-1.mga8
+ Revision: 1415508
- drop merged patches
- update to 19.1.1
- enable valgrind support

* Sat Jun 15 2019 tmb <tmb> 19.1.0-2.mga7
+ Revision: 1400029
- r300g: restore performance after RADEON_FLAG_NO_INTERPROCESS_SHARING
  was added
- radv: fix occlusion queries on VegaM
- radv: fix VK_EXT_memory_budget if one heap isn't available
- drop 'mapi: add static_date offset to EXT_dsa', not needed in 19.1

* Tue Jun 11 2019 tmb <tmb> 19.1.0-1.mga7
+ Revision: 1399848
- update to 19.1.0 final

* Sat Jun 08 2019 tmb <tmb> 19.1.0-0.rc5.2.mga7
+ Revision: 1399685
- add upstream mapi fixes for fdo#110302 (last 19.1 final blocker)
  * mapi: add static_date offset to MaxShaderCompilerThreadsKHR
  * mapi: add static_date offset to EXT_dsa
  * mapi: correctly handle the full offset table

* Wed Jun 05 2019 tmb <tmb> 19.1.0-0.rc5.1.mga7
+ Revision: 1399605
- update to 19.1.0-rc5

* Wed May 29 2019 tmb <tmb> 19.1.0-0.rc4.1.mga7
+ Revision: 1399367
- update to 19.1.0-rc4

* Sun May 26 2019 tmb <tmb> 19.1.0-0.rc3.2.mga7
+ Revision: 1399196
- Revert "mesa: unreference current winsys buffers when unbinding winsys
  buffers" as it breaks atleast blender (fdo #110721, mga #24868)
- add fixes from staging/19.1

* Tue May 21 2019 tmb <tmb> 19.1.0-0.rc3.1.mga7
+ Revision: 1398775
- update to 19.1.0-rc3

* Mon May 20 2019 tmb <tmb> 19.1.0-0.rc2.5.mga7
+ Revision: 1398560
- add fixes from staging/19.1 branch

* Sun May 19 2019 tmb <tmb> 19.1.0-0.rc2.4.mga7
+ Revision: 1398438
- meson: expose glapi through osmesa

* Fri May 17 2019 tmb <tmb> 19.1.0-0.rc2.3.mga7
+ Revision: 1398124
- add fixes from staging/19.1 branch
- enable iris driver (mga#24832)

* Thu May 16 2019 tmb <tmb> 19.1.0-0.rc2.2.mga7
+ Revision: 1397961
- add fixes from staging/19.1 branch

* Tue May 14 2019 tmb <tmb> 19.1.0-0.rc2.1.mga7
+ Revision: 1397702
- update to 19.1.0-rc2

* Sat May 11 2019 tmb <tmb> 19.1.0-0.rc1.2.mga7
+ Revision: 1397175
- drop aarch64/armv7hl filelist changes, as it got fixed by upstream
  'kmsro: add _dri.so to two of the kmsro driver'
- add current fixes from mesa/19.1 branch
- fix armv7hl filelist

* Fri May 10 2019 tmb <tmb> 19.1.0-0.rc1.1.mga7
+ Revision: 1397167
- update filelist for aarch64
- update to 19.1.0-rc1 (tv)

* Thu May 09 2019 tmb <tmb> 19.0.4-1.mga7
+ Revision: 1396967
- update to 19.0.4

* Thu May 09 2019 tmb <tmb> 19.0.3-6.mga7
+ Revision: 1396898
- add more fixes from staging/19.0 branch

* Wed May 08 2019 tmb <tmb> 19.0.3-5.mga7
+ Revision: 1396646
- add fixes from staging/19.0 branch

* Fri May 03 2019 tmb <tmb> 19.0.3-4.mga7
+ Revision: 1396317
- radv: Disable VK_EXT_descriptor_indexing

* Fri May 03 2019 tmb <tmb> 19.0.3-3.mga7
+ Revision: 1396290
- radv: apply the indexing workaround for atomic buffer operations on GFX9 (fdo #110573)
- add fixes from staging/19.0

* Sat Apr 27 2019 tmb <tmb> 19.0.3-2.mga7
+ Revision: 1395737
- add fixes from staging/19.0 branch

* Wed Apr 24 2019 tmb <tmb> 19.0.3-1.mga7
+ Revision: 1395203
- update to 19.0.3

* Tue Apr 23 2019 tmb <tmb> 19.0.2-6.mga7
+ Revision: 1394964
- drirc: Add workaround for Epic Games Launcher (fdo #110462)

* Mon Apr 22 2019 tmb <tmb> 19.0.2-5.mga7
+ Revision: 1394848
- add upstream fixes from staging/19.0 branch

* Thu Apr 18 2019 umeabot <umeabot> 19.0.2-4.mga7
+ Revision: 1392801
- Rebuild with meson 0.50.1

* Sat Apr 13 2019 tv <tv> 19.0.2-3.mga7
+ Revision: 1389757
- rebuild with llvm+clang 8.0

* Sat Apr 13 2019 akien <akien> 19.0.2-2.mga7
+ Revision: 1389663
- Enable gallium-extra-hud (GALLIUM_HUD) and lmsensors support (BR lm_sensors)
- Recommend lib64mesavulkan-drivers in main mesa package

* Thu Apr 11 2019 tmb <tmb> 19.0.2-1.mga7
+ Revision: 1388295
- update to 19.0.2

* Wed Apr 10 2019 tmb <tmb> 19.0.1-5.mga7
+ Revision: 1388163
- v3d: Bump the maximum texture size to 4k for V3D 4.x
- v3d: Don't try to use the TFU blit path if a scissor is enabled
- nir: Take if_uses into account when repairing SSA
- intel: add dependency on genxml generated files
- st/va: reverse qt matrix back to its original order

* Fri Apr 05 2019 tmb <tmb> 19.0.1-4.mga7
+ Revision: 1386055
- add fixes from staging/19.0 branch

* Fri Apr 05 2019 pterjan <pterjan> 19.0.1-3.mga7
+ Revision: 1385846
- Do not try to generate radeonsi/r600 packages when radeonsi is disabled

* Wed Mar 27 2019 tmb <tmb> 19.0.1-2.mga7
+ Revision: 1380712
- Revert "anv/radv: release memory allocated by glsl types
  during spirv_to_nir"

* Wed Mar 27 2019 tmb <tmb> 19.0.1-1.mga7
+ Revision: 1380684
- update to 19.0.1
  * drop merged patches

* Tue Mar 26 2019 akien <akien> 19.0.0-8.mga7
+ Revision: 1380388
- Rebuild against meson-0.50.0-2.mga7 with -Db_ndebug=true (rhbz#1692426)

* Sat Mar 23 2019 tmb <tmb> 19.0.0-7.mga7
+ Revision: 1379742
- softpipe: fix texture view crashes
- glsl: Cross validate variable's invariance by explicit invariance only
- radv: Fix driverUUID
- anv/radv: release memory allocated by glsl types during spirv_to_nir
- mesa: Fix GL_NUM_DEVICE_UUIDS_EXT
+ tv <tv>
- remove commented out wayland-egl -- split for quite some time
- remove very old conflicts/obsoletes tags
- disable radeonsi on arm

* Thu Mar 21 2019 tv <tv> 19.0.0-6.mga7
+ Revision: 1379343
- switch to meson
- enable opencl
- enable OMX drivers

* Wed Mar 20 2019 tmb <tmb> 19.0.0-5.mga7
+ Revision: 1379293
- rebuild for missing package

* Wed Mar 20 2019 tmb <tmb> 4.mga7-current
+ Revision: 1379265
- more fixes from staging/19.0 branch

* Wed Mar 20 2019 tmb <tmb> 19.0.0-3.mga7
+ Revision: 1379153
- add fixes from staging/19.0 branch

* Fri Mar 15 2019 tmb <tmb> 19.0.0-2.mga7
+ Revision: 1377591
- add fixes from staging/19.0 branch

* Wed Mar 13 2019 tmb <tmb> 19.0.0-1.mga7
+ Revision: 1375854
- update to 19.0.0 final

* Wed Mar 06 2019 tmb <tmb> 19.0.0-0.rc7.1.mga7
+ Revision: 1372063
- update to 19.0.0-rc7

* Wed Feb 27 2019 tv <tv> 19.0.0-0.rc6.1.mga7
+ Revision: 1370326
- 19.0.0-rc6

* Wed Feb 20 2019 tmb <tmb> 19.0.0-0.rc5.1.mga7
+ Revision: 1368658
- update to 19.0.0-rc5

* Thu Feb 14 2019 tmb <tmb> 19.0.0-0.rc4.1.mga7
+ Revision: 1366776
- update to 19.0.0-rc4

* Wed Feb 13 2019 tmb <tmb> 19.0.0-0.rc3.1.mga7
+ Revision: 1366518
- update to 19.0.0-rc3

* Wed Feb 06 2019 tmb <tmb> 19.0.0-0.rc2.1.mga7
+ Revision: 1363528
- update to 19.0.0-rc2

* Thu Jan 31 2019 tv <tv> 19.0.0-0.rc1.3.mga7
+ Revision: 1362089
- adjust arm drivers
- new release

* Thu Jan 17 2019 tmb <tmb> 18.3.2-1.mga7
+ Revision: 1357660
- update to 18.3.2

* Sun Jan 13 2019 tmb <tmb> 18.3.1-3.mga7
+ Revision: 1355920
- add current fixes from upstream staging/18.3 branch

* Thu Dec 20 2018 tmb <tmb> 18.3.1-2.mga7
+ Revision: 1343804
- pci_ids: add new VegaM pci id

* Wed Dec 12 2018 tmb <tmb> 18.3.1-1.mga7
+ Revision: 1340370
- update to 18.3.1

* Mon Dec 10 2018 akien <akien> 18.3.0-4.mga7
+ Revision: 1340092
- Rename libvulkan* to libmesavulkan*, to avoid confusion with Vulkan SDK
- Merged libvulkan_intel and libvulkan_radeon into libmesavulkan-drivers
- Build libmesavulkan-drivers on non-x86 (radeon only, intel does not build)

* Mon Dec 10 2018 akien <akien> 18.3.0-3.mga7
+ Revision: 1339690
- Split KHR headers to subpackage, required by GL, GLES and EGL

* Sat Dec 08 2018 tmb <tmb> 18.3.0-2.mga7
+ Revision: 1338948
- add more vega10 and vega20 pci ids

* Fri Dec 07 2018 tmb <tmb> 18.3.0-1.mga7
+ Revision: 1338779
- update to 18.3.0 final

* Thu Dec 06 2018 tmb <tmb> 18.3.0-0.rc6.1.mga7
+ Revision: 1338604
- update to 18.3.0-rc6

* Fri Nov 30 2018 tmb <tmb> 18.3.0-0.rc5.1.mga7
+ Revision: 1336872
- update to 18.3.0-rc5

* Thu Nov 22 2018 tmb <tmb> 18.3.0-0.rc4.1.mga7
+ Revision: 1333397
- winsys/amdgpu: fix a buffer leak in amdgpu_bo_from_handle
- update to 18.3.0-rc4

* Fri Nov 16 2018 tmb <tmb> 18.3.0-0.rc3.1.mga7
+ Revision: 1330231
- update to 18.3.0-rc3

* Fri Nov 09 2018 tv <tv> 18.3.0-0.rc2.1.mga7
+ Revision: 1329165
- 18.0.3 RC2

* Fri Nov 09 2018 tv <tv> 18.3.0-0.rc1.1.mga7
+ Revision: 1329069
- fix intel tools linking on ia32
- 18.3.0 RC1

* Wed Oct 31 2018 tmb <tmb> 18.2.4-1.mga7
+ Revision: 1327002
- update to 18.2.4

* Fri Oct 19 2018 tv <tv> 18.2.3-1.mga7
+ Revision: 1322549
- new release

* Fri Oct 05 2018 tmb <tmb> 18.2.2-1.mga7
+ Revision: 1317962
- update to 18.2.2

* Mon Sep 24 2018 tv <tv> 18.2.1-2.mga7
+ Revision: 1303461
- rebuild with llvm-7.0

* Fri Sep 21 2018 tmb <tmb> 18.2.1-1.mga7
+ Revision: 1294730
- update to 18.2.1

* Sat Sep 08 2018 tmb <tmb> 18.2.0-3.mga7
+ Revision: 1257897
- add fixes from staging/18.2 branch

* Sat Sep 08 2018 tmb <tmb> 18.2.0-2.mga7
+ Revision: 1257893
- mesa: enable ARB_vertex_buffer_object in core profile

* Sat Sep 08 2018 tmb <tmb> 18.2.0-1.mga7
+ Revision: 1257739
- update to 18.2.0 final

* Thu Sep 06 2018 tv <tv> 18.2.0-0.rc6.1.mga7
+ Revision: 1257439
- new release

* Thu Aug 30 2018 tmb <tmb> 18.2.0-0.rc5.1.mga7
+ Revision: 1255754
- update to 18.2.0-rc5

* Wed Aug 22 2018 tv <tv> 18.2.0-0.rc4.1.mga7
+ Revision: 1253485
- 18.2.0 RC4

* Tue Aug 21 2018 tv <tv> 18.2.0-0.rc3.1.mga7
+ Revision: 1253321
- BR pkgconfig(xrandr)
- new release

* Mon Aug 13 2018 tmb <tmb> 18.1.6-1.mga7
+ Revision: 1251402
- update to 18.1.6

* Fri Jul 27 2018 tmb <tmb> 18.1.5-1.mga7
+ Revision: 1245561
- update to 18.1.5

* Fri Jul 13 2018 tmb <tmb> 18.1.4-1.mga7
+ Revision: 1243496
- update to 18.1.4

* Mon Jul 02 2018 tv <tv> 18.1.3-1.mga7
+ Revision: 1241276
- new release

* Sat Jun 16 2018 tv <tv> 18.1.2-1.mga7
+ Revision: 1237202
- new release

* Fri Jun 01 2018 tv <tv> 18.1.1-1.mga7
+ Revision: 1233505
- 18.1.1

* Sun May 20 2018 kekepower <kekepower> 18.1.0-1.mga7
+ Revision: 1230842
- Update to version 18.1.0

* Sat May 12 2018 tv <tv> 18.1.0-0.rc4.1.mga7
+ Revision: 1228553
- 18.1.0 RC4

* Sat May 05 2018 tv <tv> 18.1.0-0.rc3.1.mga7
+ Revision: 1226617
- 18.1.0 RC3

* Tue May 01 2018 tv <tv> 18.1.0-0.rc2.1.mga7
+ Revision: 1224299
- 18.1.0 RC2
- BR py2 modules
- 18.1.0 RC1

* Thu Apr 19 2018 tv <tv> 18.0.1-1.mga7
+ Revision: 1220040
- new release

* Mon Apr 16 2018 tv <tv> 18.0.0-8.mga7
+ Revision: 1219406
- Re-enable wayland support, but still drop mesa-wayland-egl subpackage

* Mon Apr 16 2018 ovitters <ovitters> 18.0.0-7.mga7
+ Revision: 1219359
- reenable Wayland

* Wed Apr 11 2018 tv <tv> 18.0.0-6.mga7
+ Revision: 1217666
- vaapi cannot be disabled
- temporary disable vaapi as libllvm5 was removed too fast
- really rebuild with llvm 6.0

* Wed Apr 11 2018 tmb <tmb> 18.0.0-4.mga7
+ Revision: 1217618
- rebuild as mesa/llvm/.. move downgraded last arm fixes

* Tue Apr 10 2018 neoclust <neoclust> 18.0.0-3.mga7
+ Revision: 1216966
- Enable arm drivers

* Tue Apr 03 2018 tv <tv> 18.0.0-2.mga7
+ Revision: 1214970
- disable wayland libs (moved to wayland 1.15)
- Drop S3TC build hack

* Wed Mar 28 2018 tv <tv> 18.0.0-1.mga7
+ Revision: 1213070
- new release

* Wed Mar 21 2018 tmb <tmb> 18.0.0-0.rc5.2.mga7
+ Revision: 1211143
- rebuild for arm

* Wed Mar 21 2018 tv <tv> 18.0.0-0.rc5.1.mga7
+ Revision: 1211116
- 18.0.0 RC5

* Wed Mar 21 2018 tv <tv> 18.0.0-0.rc4.2.mga7
+ Revision: 1211039
- rebuild with llvm 6.0

* Fri Feb 09 2018 tmb <tmb> 18.0.0-0.rc4.1.mga7
+ Revision: 1199811
- update to 18.0.0-rc4

* Thu Feb 01 2018 wally <wally> 18.0.0-0.rc3.2.mga7
+ Revision: 1198493
- drop tainted build leftovers from .spec as unneeded (S3TC patent has expired)

* Mon Jan 29 2018 tv <tv> 18.0.0-0.rc3.1.mga7.tainted
+ Revision: 1198003
- 18.0.0 RC3

* Sat Jan 20 2018 tv <tv> 17.3.3-2.mga7
+ Revision: 1195049
- new release

* Tue Jan 16 2018 tmb <tmb> 17.3.2-2.mga7
+ Revision: 1193753
- drop: 'i965: expose SRGB visuals and turn on EGL_KHR_gl_colorspace' as
  it breaks more than it fixes

* Tue Jan 09 2018 tv <tv> 17.3.2-1.mga7
+ Revision: 1191955
- new release

* Thu Dec 21 2017 tmb <tmb> 17.3.1-1.mga7
+ Revision: 1183687
- update to 17.3.1

* Fri Dec 08 2017 tmb <tmb> 17.3.0-1.mga7.tainted
+ Revision: 1181748
- update to 17.3.0 final

* Mon Dec 04 2017 tv <tv> 17.3.0-0.rc6.1.mga7
+ Revision: 1181234
- 17.3 RC6

* Mon Nov 20 2017 tmb <tmb> 17.3.0-0.rc5.1.mga7.tainted
+ Revision: 1177988
- update to rc5

* Tue Nov 14 2017 tv <tv> 17.3.0-0.rc4.3.mga7
+ Revision: 1177373
- bump release b/c BS wrongly send to tainted

* Tue Nov 14 2017 tv <tv> 17.3.0-0.rc4.2.mga7.tainted
+ Revision: 1177357
- new release

* Thu Nov 09 2017 tmb <tmb> 17.3.0-0.rc3.2.mga7.tainted
+ Revision: 1176680
- i965: expose SRGB visuals and turn on EGL_KHR_gl_colorspace

* Tue Nov 07 2017 tv <tv> 17.3.0-0.rc3.1.mga7.tainted
+ Revision: 1176388
- 17.3.0 RC3

* Sat Nov 04 2017 tv <tv> 17.3.0-0.rc2.1.mga7
+ Revision: 1175798
- new release

* Sat Nov 04 2017 tv <tv> 17.2.4-1.mga7
+ Revision: 1175796
- new release

* Tue Oct 31 2017 tv <tv> 17.2.3-5.mga7
+ Revision: 1175167
- rebuild with LLVM-5

* Sat Oct 28 2017 cjw <cjw> 17.2.3-4.mga7
+ Revision: 1174481
- disable x86-specific gallium driver svga for arm builds

* Wed Oct 25 2017 tv <tv> 17.2.3-3.mga7
+ Revision: 1173701
- always enable S3TC

* Sat Oct 21 2017 tv <tv> 17.2.3-2.mga7
+ Revision: 1172969
- rebuild with llvm-5

* Thu Oct 19 2017 tv <tv> 17.2.3-1.mga7
+ Revision: 1172771
- new release

* Tue Oct 03 2017 tv <tv> 17.2.2-2.mga7.tainted
+ Revision: 1167695
- tainted build

* Tue Oct 03 2017 tv <tv> 17.2.2-1.mga7
+ Revision: 1167662
- new release

* Fri Sep 22 2017 tv <tv> 17.2.1-3.2.mga7
+ Revision: 1156949
- rebuild with llvm-4.0 in core/release

* Thu Sep 21 2017 tv <tv> 17.2.1-3.1.mga7.tainted
+ Revision: 1156923
- rebuild with llvm-4.0 in core/release

* Thu Sep 21 2017 tv <tv> 17.2.1-3.mga7.tainted
+ Revision: 1156912
- rebuild with llvm-4.0

* Mon Sep 18 2017 tv <tv> 17.2.1-2.mga7.tainted
+ Revision: 1155195
- tainted build

* Mon Sep 18 2017 tv <tv> 17.2.1-1.mga7
+ Revision: 1155144
- new release

* Tue Sep 05 2017 tv <tv> 17.2.0-2.mga7.tainted
+ Revision: 1151432
- bump release for tainted build

* Tue Sep 05 2017 tv <tv> 17.2.0-1.mga7
+ Revision: 1151285
- new release

* Sat Sep 02 2017 tmb <tmb> 17.2.0-0.rc5.4.mga7.tainted
+ Revision: 1150745
- rebuild with new gcc/binutils

* Sat Sep 02 2017 tmb <tmb> 17.2.0-0.rc5.3.mga7
+ Revision: 1150684
- rebuild with new gcc/binutils

* Tue Aug 22 2017 tv <tv> 17.2.0-0.rc5.2.mga7.tainted
+ Revision: 1142914
- tainted build

* Tue Aug 22 2017 tv <tv> 17.2.0-0.rc5.1.mga7
+ Revision: 1142889
- new release

* Mon Aug 14 2017 tv <tv> 17.2.0-0.rc4.2.mga7
+ Revision: 1140200
- tainted rebuild

* Mon Aug 14 2017 tv <tv> 17.2.0-0.rc4.1.mga7
+ Revision: 1140194
- BR wayland-protocols
- adjust renamed options in order to get rid of warnings
- BS doesn't handle tilde in VR, so revert r1100119
- new release

* Sun Jul 16 2017 tmb <tmb> 17.1.5-1.mga7.tainted
+ Revision: 1123864
- update to 17.1.5

* Fri Jun 30 2017 tmb <tmb> 17.1.4-1.mga6.tainted
+ Revision: 1108762
- update to 17.1.4

* Tue Jun 20 2017 tmb <tmb> 17.1.3-1.mga6.tainted
+ Revision: 1107908
- 17.1.3 (bug and crash fixes)

* Tue Jun 06 2017 tmb <tmb> 17.1.2-1.mga6.tainted
+ Revision: 1107097
- update to 17.1.2 final
- update to 17.1.2 final

* Sat Jun 03 2017 tmb <tmb> 17.1.1-2.mga6.tainted
+ Revision: 1106363
- add current fixes for upcoming 17.1.2 from upstream 17.1 branch

* Thu May 25 2017 tmb <tmb> 17.1.1-1.mga6.tainted
+ Revision: 1104553
- update to 17.1.1

* Wed May 10 2017 akien <akien> 17.1.0-1.mga6.tainted
+ Revision: 1100119
- Version 17.1.0

* Mon May 08 2017 akien <akien> 17.0.5-3.mga6.tainted
+ Revision: 1099595
- Enable RADV build (radeon/amdgpu vulkan driver) (mga#20802)
- Fix conditional support for rc tarballs

* Sat Apr 29 2017 tmb <tmb> 17.0.5-2.mga6.tainted
+ Revision: 1098008
- submit to tainted

* Sat Apr 29 2017 tmb <tmb> 17.0.5-1.mga6
+ Revision: 1098007
- update to 17.0.5

* Wed Apr 26 2017 tmb <tmb> 17.0.4-3.mga6.tainted
+ Revision: 1097755
- submit to tainted

* Wed Apr 26 2017 tmb <tmb> 17.0.4-2.mga6
+ Revision: 1097740
- Revert "i915: Always enable GL 2.0 support."

* Mon Apr 24 2017 ghibo <ghibo> 17.0.4-1.mga6.tainted
+ Revision: 1097193
- Update to release 17.0.4 (among bugfixes, fixes upstrem memleak bug #100452).

* Sat Apr 01 2017 tmb <tmb> 17.0.3-2.mga6.tainted
+ Revision: 1095601
- submit to tainted

* Sat Apr 01 2017 tmb <tmb> 17.0.3-1.mga6
+ Revision: 1095596
- update to 17.0.3

* Tue Mar 28 2017 tmb <tmb> 17.0.2-3.mga6.tainted
+ Revision: 1095010
- rebuild with new gcc & llvm

* Tue Mar 21 2017 tmb <tmb> 17.0.2-2.mga6.tainted
+ Revision: 1093849
- update to 17.0.2

* Mon Mar 20 2017 tmb <tmb> 17.0.2-1.mga6
+ Revision: 1093839
- update to 17.0.2

* Sun Mar 05 2017 tmb <tmb> 17.0.1-2.mga6.tainted
+ Revision: 1088779
- update to 17.0.1

* Sun Mar 05 2017 tmb <tmb> 17.0.1-1.mga6
+ Revision: 1088772
- update to 17.0.1
- update to 13.0.5

* Mon Feb 20 2017 tmb <tmb> 13.0.5-1.mga6
+ Revision: 1087054
- update to 13.0.5

* Thu Feb 02 2017 tmb <tmb> 13.0.4-2.mga6.tainted
+ Revision: 1084580
- update to 13.0.4

* Thu Feb 02 2017 tmb <tmb> 13.0.4-1.mga6
+ Revision: 1084570
- update to 13.0.4

* Sat Jan 07 2017 tmb <tmb> 13.0.3-4.mga6.tainted
+ Revision: 1080487
- rebuild with new llvm

* Sat Jan 07 2017 tmb <tmb> 13.0.3-3.mga6
+ Revision: 1080474
- rebuild with new llvm

* Thu Jan 05 2017 tmb <tmb> 13.0.3-2.mga6.tainted
+ Revision: 1080248
- submit to tainted

* Thu Jan 05 2017 tmb <tmb> 13.0.3-1.mga6
+ Revision: 1080242
- update to 13.0.3

* Fri Dec 09 2016 tv <tv> 13.0.2-2.mga6.tainted
+ Revision: 1073752
- fixes random radeonsi GPU hangs (bfo#93649)

* Mon Nov 28 2016 tmb <tmb> 13.0.2-1.mga6.tainted
+ Revision: 1070654
- update to 13.0.2

* Mon Nov 14 2016 tmb <tmb> 13.0.1-2.mga6.tainted
+ Revision: 1067148
- submit to tainted

* Mon Nov 14 2016 tmb <tmb> 13.0.1-1.mga6
+ Revision: 1067142
- update to 13.0.1
  * drop merged/obsolete patches
+ ghibo <ghibo>
- Prepare for upcoming 13.0.1.
- The %%nil construct for %%relc (release candidate) no longer works, reworks the conditional check (relc had to be really undefined).
- Update make-git-snapshot.sh to produce xz files.
- Merged patches 169-171 from git for stable too
- Added further patches from git
- Merged fixes from git for 13.0 stable.

* Mon Nov 07 2016 akien <akien> 13.0.0-2.1.mga6.tainted
+ Revision: 1065701
- Rebuild for tainted

* Mon Nov 07 2016 akien <akien> 13.0.0-2.mga6
+ Revision: 1065668
- Rebuild in tainted
+ ghibo <ghibo>
- Revert usage of %%{_arch} for vulkan .json file
- Use %%{_arch} for .json file, as suggested by Charles Edwards.
- Update to release 13.0.0

* Thu Sep 15 2016 akien <akien> 12.0.3-1.1.mga6.tainted
+ Revision: 1053152
- Build for tainted

* Thu Sep 15 2016 shlomif <shlomif> 12.0.3-1.mga6
+ Revision: 1053032
- New version: 12.0.3.
+ tv <tv>
- patch 5: fd leak fix

* Mon Sep 12 2016 tv <tv> 12.0.2-2.1.mga6.tainted
+ Revision: 1051663
- bump for tainted/release

* Mon Sep 12 2016 tv <tv> 12.0.2-2.mga6
+ Revision: 1051651
- remove support for disabling hardware gallium
--enable-gallium-egl is no more
- switch to %%autosetup & simplify
- use gallium-osmesa instead of classic osmesa
  (the former handles openGl 3+ whereas the later is limited to openGl 2.1)
- stop doing a second build for osmesa
- don't package useless wglext.h
- remove disabled support for openVG as it has been removed upstream anyway
- use %%make_install

* Sun Sep 11 2016 tv <tv> 12.0.2-1.1.mga6.tainted
+ Revision: 1051534
- bump for tainted/release

* Fri Sep 09 2016 tv <tv> 12.0.2-1.mga6
+ Revision: 1051000
- new release
- drop merged patches

* Sat Jul 23 2016 tmb <tmb> 12.0.1-6.mga6.tainted
+ Revision: 1043473
- submit to tainted

* Sat Jul 23 2016 tmb <tmb> 12.0.1-5.mga6
+ Revision: 1043472
- Revert "gallium: Force blend color to 16-byte alignment"
- Avoid overflow in 'last' variable of FindGLXFunction(...)
- mesa: Don't call GenerateMipmap if Width or Height == 0

* Wed Jul 13 2016 tmb <tmb> 12.0.1-4.mga6.tainted
+ Revision: 1041917
- submit to tainted

* Wed Jul 13 2016 tmb <tmb> 12.0.1-3.mga6
+ Revision: 1041913
- BR: libgcrypt for vulcan support
+ akien <akien>
- Enable Vulkan driver (anvil) for intel

* Sun Jul 10 2016 tmb <tmb> 12.0.1-2.mga6.tainted
+ Revision: 1040832
- submit to tainted

* Sun Jul 10 2016 tmb <tmb> 12.0.1-1.mga6
+ Revision: 1040746
- update to 12.0.1

* Fri Jul 08 2016 tmb <tmb> 12.0.0-2.mga6.tainted
+ Revision: 1039888
- submit to tainted

* Fri Jul 08 2016 tmb <tmb> 12.0.0-1.mga6
+ Revision: 1039883
- update to 12.0.0 final

* Thu Jul 07 2016 tmb <tmb> 12.0.0~rc4-9.mga6.tainted
+ Revision: 1039523
- submit to tainted

* Thu Jul 07 2016 tmb <tmb> 12.0.0~rc4-8.mga6
+ Revision: 1039522
- sync in fixes from upstream mesa 12.0 branch

* Tue Jul 05 2016 tmb <tmb> 12.0.0~rc4-7.mga6.tainted
+ Revision: 1039019
- submit to tainted

* Tue Jul 05 2016 tmb <tmb> 12.0.0~rc4-6.mga6
+ Revision: 1039010
- mapi: Export all GLES 3.1 functions in libGLESv2.so
- i965: Avoid division by zero
- i965: Add more Kabylake PCI IDs
  . i965: Removing PCI IDs that are no longer listed as Kabylake
- radeonsi: use DRAW_(INDEX_)INDIRECT_MULTI on Polaris
- radeonsi: drop the DRAW_PREAMBLE packet on Polaris
- radeonsi: set PA_SU_SMALL_PRIM_FILTER_CNTL register on Polaris
- radeonsi: fix fractional odd tessellation spacing for Polaris

* Sat Jul 02 2016 wally <wally> 12.0.0~rc4-5.mga6.tainted
+ Revision: 1038359
- enable txc-dxtn requires again

* Sat Jul 02 2016 wally <wally> 12.0.0~rc4-4.mga6.tainted
+ Revision: 1038290
- bump rel for tainted build

* Sat Jul 02 2016 wally <wally> 12.0.0~rc4-3.mga6
+ Revision: 1038283
- temporary disable txc-dxtn requires on arm to allow building it

* Tue Jun 21 2016 tmb <tmb> 12.0.0~rc4-2.mga6.tainted
+ Revision: 1037091
- update to 12.0.0 rc4

* Tue Jun 21 2016 tmb <tmb> 12.0.0~rc4-1.mga6
+ Revision: 1037090
- update to 12.0.0 rc4

* Wed Jun 15 2016 tmb <tmb> 12.0.0~rc3-2.mga6.tainted
+ Revision: 1021526
- submit to tainted

* Wed Jun 15 2016 tmb <tmb> 12.0.0~rc3-1.mga6
+ Revision: 1021521
- update to 12.0.0 rc3

* Tue Jun 07 2016 tmb <tmb> 12.0.0~rc2-2.mga6.tainted
+ Revision: 1020671
- submit to tainted

* Tue Jun 07 2016 tmb <tmb> 12.0.0~rc2-1.mga6
+ Revision: 1020629
- drop merged patches
+ tv <tv>
- 12.0 RC2

* Fri Jun 03 2016 tmb <tmb> 12.0.0~rc1-4.mga6.tainted
+ Revision: 1020109
- submit to tainted

* Fri Jun 03 2016 tmb <tmb> 12.0.0~rc1-3.mga6
+ Revision: 1020108
- add post -rc1 fixes from mesa 12.0 branch

* Tue May 31 2016 tmb <tmb> 12.0.0~rc1-2.mga6.tainted
+ Revision: 1019642
- submit to tainted

* Tue May 31 2016 tmb <tmb> 12.0.0~rc1-1.mga6
+ Revision: 1019622
- BR: python3
- update to 12.0.0-rc1

* Mon May 09 2016 tv <tv> 11.2.2-2.mga6.tainted
+ Revision: 1011792
- drop merged patches
- new release

* Mon May 09 2016 tmb <tmb> 11.2.1-2.mga6.tainted
+ Revision: 1011249
- sync in fixes from upstream 11.2 branch

* Mon Apr 18 2016 tv <tv> 11.2.1-1.mga6.tainted
+ Revision: 1003349
- new release

* Mon Apr 04 2016 tv <tv> 11.2.0-1.mga6.tainted
+ Revision: 998395
- new release

* Wed Mar 30 2016 tv <tv> 11.2.0~rc4-2.mga6.tainted
+ Revision: 996814
- bump release for tainted

* Wed Mar 30 2016 tv <tv> 11.2.0~rc4-1.mga6
+ Revision: 996792
- new release

* Mon Mar 14 2016 tv <tv> 11.2.0~rc3-3.mga6
+ Revision: 990561
- submit to core/release

* Sat Mar 12 2016 tv <tv> 11.2.0~rc3-2.mga6
+ Revision: 989411
- rebuild with new clang
+ akien <akien>
- Bump OpenGL version in descriptions to 4.1

* Tue Mar 08 2016 tv <tv> 11.2.0~rc3-1.mga6
+ Revision: 987318
- 11.2.0 RC3

* Thu Feb 11 2016 tv <tv> 11.1.2-1.mga6
+ Revision: 953748
- 11.1.2

* Wed Jan 13 2016 tmb <tmb> 11.1.1-2.mga6.tainted
+ Revision: 922844
- rebuild for tainted

* Wed Jan 13 2016 tmb <tmb> 11.1.1-1.mga6
+ Revision: 922766
- update to 11.1.1

* Fri Jan 08 2016 tmb <tmb> 11.1.0-9.mga6.tainted
+ Revision: 920663
- rebuild for tainted

* Fri Jan 08 2016 tmb <tmb> 11.1.0-8.mga6
+ Revision: 920659
- BR: python-mako
- pull in fixes from upstream 11.1 branch

* Wed Jan 06 2016 tmb <tmb> 11.1.0-6.mga6.tainted
+ Revision: 920078
+ rebuild (emptylog)

* Wed Jan 06 2016 tmb <tmb> 11.1.0-5.mga6
+ Revision: 920067
- rebuild with new llvm

* Tue Dec 22 2015 tv <tv> 11.1.0-2.mga6
+ Revision: 913413
- enable virgl driver

* Wed Dec 16 2015 tv <tv> 11.1.0-1.mga6.tainted
+ Revision: 911141
+ rebuild (emptylog)

* Wed Dec 16 2015 tv <tv> 11.1.0-1.mga6
+ Revision: 911140
- fix build in !RC case
- new release

* Tue Dec 08 2015 tv <tv> 11.1.0~rc3-1.mga6.tainted
+ Revision: 908805
- 11.0.0 RC3

* Tue Dec 01 2015 tv <tv> 11.1.0~rc2-1.mga6.tainted
+ Revision: 907450
- drop merged patches
- new release

* Mon Nov 23 2015 tv <tv> 11.1.0~rc1-1.mga6.tainted
+ Revision: 905096
- build fixes from git
- enable to use tilde in version
- 11.1.0 RC1

* Sat Nov 21 2015 tv <tv> 11.0.6-1.mga6.tainted
+ Revision: 904712
- new release

* Wed Nov 11 2015 tmb <tmb> 11.0.5-1.mga6.tainted
+ Revision: 902580
- update to 11.0.5
+ blino <blino>
- rename hardware flag as hardware_gallium and make it a bcond_without

* Mon Nov 02 2015 blino <blino> 11.0.4-3.mga6
+ Revision: 897332
- do not package Direct3D9 (nine), xatracker and vdpau drivers when hardware is disabled
  (D3D9 needs at least one non-swrast gallium driver)
- do not buildrequire llvm when hardware support is disabled
- remove unused bootstrap flag

* Sun Oct 25 2015 tv <tv> 11.0.4-2.mga6.tainted
+ Revision: 895361
- new release
- drop support for Alpha/ia64/PPC/Sparc
- kill old llvm hack that is no more needed

* Wed Oct 14 2015 tv <tv> 11.0.3-2.mga6
+ Revision: 891317
- try building w/o limiting to 8 CPUs

* Sat Oct 10 2015 tmb <tmb> 11.0.3-1.mga6.tainted
+ Revision: 889418
- update to 11.0.3

* Tue Sep 29 2015 tv <tv> 11.0.2-1.mga6.tainted
+ Revision: 884928
- new release

* Sat Sep 26 2015 tv <tv> 11.0.1-1.mga6.tainted
+ Revision: 883983
- new release

* Wed Sep 23 2015 tmb <tmb> 11.0.0-2.mga6.tainted
+ Revision: 882320
- rebuild with new llvm

* Sun Sep 20 2015 tv <tv> 11.0.0-1.mga6.tainted
+ Revision: 881766
- new release

* Thu Sep 10 2015 tv <tv> 10.6.7-1.mga6.tainted
+ Revision: 875761
- new release

* Sat Sep 05 2015 tv <tv> 10.6.6-1.mga6.tainted
+ Revision: 873068
- new release

* Sat Aug 22 2015 tmb <tmb> 10.6.5-1.mga6.tainted
+ Revision: 868142
- update to 10.6.5

* Fri Aug 21 2015 tmb <tmb> 10.6.4-5.mga6.tainted
+ Revision: 867059
- rebuild for new gcc/llvm

* Thu Aug 20 2015 tmb <tmb> 10.6.4-4.mga6
+ Revision: 866715
- rebuild with new llvm

* Tue Aug 18 2015 tmb <tmb> 10.6.4-3.mga6.tainted
+ Revision: 865425
- rebuild with new glibc

* Tue Aug 11 2015 tv <tv> 10.6.4-2.mga6.tainted
+ Revision: 863198
- new release

* Thu Jul 30 2015 tv <tv> 10.6.3-2.mga6.tainted
+ Revision: 859059
- rebuild with new llvm

* Mon Jul 27 2015 tv <tv> 10.6.3-1.mga6.tainted
+ Revision: 858186
- new release

* Sat Jul 11 2015 tmb <tmb> 10.6.2-1.mga6.tainted
+ Revision: 853335
- update to 10.6.2

* Mon Jun 29 2015 tv <tv> 10.6.1-1.mga6.tainted
+ Revision: 847562
- new release

* Sun Jun 21 2015 tmb <tmb> 10.6.0-1.mga6.tainted
+ Revision: 837893
- update to 10.6.0
  * drop merged patches

* Sat Jun 13 2015 tmb <tmb> 10.5.7-3.mga5.tainted
+ Revision: 823068
- i965: Fix HW blitter pitch limits
- i915: Blit RGBX<->RGBA drawpixels
- i965: Export format comparison for blitting between miptrees
- i965: do_blit_drawpixels: decode array formats

* Fri Jun 12 2015 tmb <tmb> 10.5.7-2.mga5.tainted
+ Revision: 823010
- 10.5.7 (better hw support, llvm fixes

* Tue Mar 24 2015 tmb <tmb> 10.4.7-1.mga5.tainted
+ Revision: 819200
- update to 10.4.7 final

* Fri Mar 20 2015 tmb <tmb> 10.4.6-3.mga5.tainted
+ Revision: 819049
- sync in upstream fixes from upcoming 10.4.7

* Wed Mar 11 2015 tmb <tmb> 10.4.6-2.mga5.tainted
+ Revision: 818325
- gallium/auxiliary/indices: fix start param
- egl: Take alpha bits into account when selecting GBM formats
- r300g: Use PATH_MAX instead of limiting ourselves to 100 chars
- r300g: Check return value of snprintf()
- mesa: rename format_info.c to format_info.h
- i965/vec4: Don't lose the saturate modifier in copy propagation

* Fri Mar 06 2015 tmb <tmb> 10.4.6-1.mga5.tainted
+ Revision: 817931
- 10.4.6 (maintenance release)

* Sat Feb 21 2015 tmb <tmb> 10.4.5-1.mga5.tainted
+ Revision: 816181
- update to 10.4.5

* Sat Feb 07 2015 tv <tv> 10.4.4-2.mga5.tainted
+ Revision: 813873
- new release
- drop merged patches

* Wed Feb 04 2015 tmb <tmb> 10.4.3-2.mga5.tainted
+ Revision: 813462
- mesa: fix display list 8-byte alignment issue
- glx/dri3: Request non-vsynced Present for swapinterval zero
- i965: Fix max_wm_threads for CHV
- configure: Link against all LLVM targets when building clover
- egl: Pass the correct X visual depth to xcb_put_image()
- gallium/util: Don't use __builtin_clrsb in util_last_bit()
- st/osmesa: Fix osbuffer->textures indexing

* Sat Jan 24 2015 tv <tv> 10.4.3-1.mga5.tainted
+ Revision: 812143
- enable Nine state-tracker (Direct3D9 API)
- enable VA state-tracker
- new release

* Tue Jan 13 2015 tmb <tmb> 10.4.2-1.mga5.tainted
+ Revision: 810327
- 10.4.2 (maintenance release)

* Thu Jan 08 2015 tmb <tmb> 10.4.1-2.mga5.tainted
+ Revision: 809313
- i965: Add missing BRW_NEW_*_PROG_DATA to texture/renderbuffer atoms.
- radeonsi: Don't modify PA_SC_RASTER_CONFIG register value if rb_mask == 0
- nv50,nvc0: implement half_pixel_center
- nv50,nvc0: set vertex id base to index_bias
- i965: Fix start/base_vertex_location for >1 prims but !BRW_NEW_VERTICES.
- glsl_to_tgsi: fix a bug in copy propagation
- i965: Use safer pointer arithmetic in intel_texsubimage_tiled_memcpy()
- i965: Use safer pointer arithmetic in gather_oa_results()

* Tue Dec 30 2014 tmb <tmb> 10.4.1-1.mga5.tainted
+ Revision: 807332
- update to 10.4.1
- drop merged patches

* Sun Dec 14 2014 tmb <tmb> 10.4.0-5.mga5.tainted
+ Revision: 803109
- glx/dri3: Fix glXWaitForSbcOML() to handle targetSBC==0 correctly.
- glx/dri3: Track separate (ust, msc) for PresentPixmap vs. PresentNotifyMsc.
- glx/dri3: Request non-vsynced Present for swapinterval zero.
- glx/dri3: Don't fail on glXSwapBuffersMscOML(dpy, window, 0, 0, 0).

* Sun Dec 14 2014 tmb <tmb> 10.4.0-4.mga5.tainted
+ Revision: 803033
- update to 10.4.0 final

* Sun Dec 14 2014 tmb <tmb> 10.4.0-3.rc4.3.mga5.tainted
+ Revision: 803004
- sync in upstream fixes

* Sat Dec 13 2014 tmb <tmb> 10.4.0-3.rc4.2.mga5.tainted
+ Revision: 802914
- rebuild without '~' in release

* Sun Dec 07 2014 tv <tv> 10.4.0-2~rc4.1.mga5.tainted
+ Revision: 802051
- RC4
+ neoclust <neoclust>
- Enable vg , this is needed for qtbase5

* Sat Nov 29 2014 tv <tv> 10.4.0-2~rc3.1.mga5.tainted
+ Revision: 799832
- new release

* Tue Nov 25 2014 tv <tv> 10.4.0-2~rc2.1.mga5.tainted
+ Revision: 799294
- use tilde for RCs

* Tue Nov 25 2014 tv <tv> 10.4.0-1.rc2.1.mga5
+ Revision: 799259
- new release

* Sat Nov 22 2014 tv <tv> 10.3.4-2.mga5.tainted
+ Revision: 798308
- new release

* Thu Nov 13 2014 tv <tv> 10.3.3-2.mga5.tainted
+ Revision: 796732
- new release

* Sat Oct 25 2014 tv <tv> 10.3.2-2.mga5.tainted
+ Revision: 793202
+ rebuild (emptylog)

* Sat Oct 25 2014 tv <tv> 10.3.2-2.mga5
+ Revision: 793170
- new release
- use new way to limit max number of CPUs
- bump it from 6 to 8

* Wed Oct 15 2014 umeabot <umeabot> 10.3.1-2.mga5.tainted
+ Revision: 747656
- Second Mageia 5 Mass Rebuild

* Mon Oct 13 2014 tv <tv> 10.3.1-1.mga5.tainted
+ Revision: 738481
- new release
- rebuild for missing pythoneggs deps

* Sat Sep 20 2014 tv <tv> 10.3.0-1.mga5.tainted
+ Revision: 697450
- adjust file list
- new release

* Sat Sep 06 2014 tv <tv> 10.3.0-0.rc3.1.mga5.tainted
+ Revision: 672548
- 10.3 RC3

* Fri Sep 05 2014 tmb <tmb> 10.3.0-0.rc2.3.mga5.tainted
+ Revision: 672235
- rebuild with new llvm

* Mon Sep 01 2014 tv <tv> 10.3.0-0.rc2.2.mga5
+ Revision: 670454
- 10.3.0 RC2

* Mon Aug 25 2014 tv <tv> 10.3.0-0.rc1.2.mga5.tainted
+ Revision: 667296
- parralel build fails with -j24 => hardcode -j4
- adjust file list
- new release

* Wed Aug 20 2014 tv <tv> 10.2.6-2.mga5.tainted
+ Revision: 665817
- new release

* Tue Aug 12 2014 blino <blino> 10.2.5-2.mga5.tainted
+ Revision: 662128
- 10.2.5

* Tue Aug 05 2014 blino <blino> 10.2.4-2.mga5.tainted
+ Revision: 659717
- rebuild for xcb-present 1.11

* Sat Jul 19 2014 tv <tv> 10.2.4-1.mga5.tainted
+ Revision: 653699
- new release
- revert "relax needlessly strict llvm BR version requirement"
  we really need llvm-3.4+ for radeonsi
  (see http://cgit.freedesktop.org/mesa/mesa/log/?h=10.2&qt=grep&q=llvm+3.4)

* Tue Jul 08 2014 tv <tv> 10.2.3-1.mga5.tainted
+ Revision: 650618
- new release
- new release
+ luigiwalser <luigiwalser>
- enable glx-tls since it is enabled in x11-server (this must match)

* Wed Jun 25 2014 tv <tv> 10.2.2-1.mga5.tainted
+ Revision: 639551
- new release

* Mon Jun 16 2014 tmb <tmb> 10.2.1-3.mga5.tainted
+ Revision: 637662
- rebuild for new llvm

* Sun Jun 08 2014 tv <tv> 10.2.1-2.mga5.tainted
+ Revision: 634695
- enable vmwgfx/svga driver for vmware

* Sun Jun 08 2014 tv <tv> 10.2.1-1.mga5
+ Revision: 634669
- enable XA state tracker for vmware driver

* Sun Jun 08 2014 tv <tv> 10.2.1-0.1.mga5.tainted
+ Revision: 634652
- new release

* Mon Jun 02 2014 tv <tv> 10.2.0-0.rc5.0.1.mga5.tainted
+ Revision: 630596
- new release

* Mon May 26 2014 tv <tv> 10.2.0-0.rc4.0.1.mga5.tainted
+ Revision: 626598
- new release

* Thu May 22 2014 tv <tv> 10.2.0-0.rc3.0.1.mga5.tainted
+ Revision: 624860
- new RC
+ tmb <tmb>
- rebuild for fixed llvm

* Tue May 13 2014 tmb <tmb> 10.2.0-0.rc2.0.2.mga5.tainted
+ Revision: 622587
- rebuild with gcc 4.9

* Sat May 10 2014 tv <tv> 10.2.0-0.rc2.0.1.mga5.tainted
+ Revision: 621812
- new RC

* Sat May 03 2014 tv <tv> 10.2.0-0.rc1.0.1.mga5.tainted
+ Revision: 619803
- new RC

* Fri Apr 25 2014 tv <tv> 10.1.1-2.mga5.tainted
+ Revision: 618024
- new release

* Wed Mar 05 2014 tv <tv> 10.1.0-2.mga5.tainted
+ Revision: 600225
- new release 10.1.0 final

* Sat Mar 01 2014 tv <tv> 10.1.0-0.rc3.2.mga5.tainted
+ Revision: 598444
- 10.1.0 RC3

* Sat Feb 22 2014 tv <tv> 10.1.0-0.rc2.2.mga5.tainted
+ Revision: 595503
- 10.1.0 RC2

* Sat Feb 08 2014 tv <tv> 10.1.0-0.rc1.2.mga5.tainted
+ Revision: 586838
- rebuild for new llvm

* Sat Feb 08 2014 tv <tv> 10.1.0-0.rc1.1.mga5.tainted
+ Revision: 585825
- BR pkgconfig(xcb-dri3) & pkgconfig(xcb-present)
- bump required libdrm
- new release
- BR xshmfence

* Tue Feb 04 2014 tv <tv> 10.0.3-2.mga5.tainted
+ Revision: 581595
- new release
- drop merged patches

* Thu Jan 09 2014 tmb <tmb> 10.0.2-1.mga4.tainted
+ Revision: 566020
- i965: Don't do the temporary-and-blit-copy for INVALIDATE_RANGE maps
- i965: Fix handling of MESA_pack_invert in blit (PBO) readpixels
- i965: fold offset into coord for textureOffset(gsampler2DRect)
- drop merged patces
- update to 10.0.2

* Sun Jan 05 2014 tmb <tmb> 10.0.1-2.mga4.tainted
+ Revision: 564877
- r600g/sb: fix stack size computation on evergreen
- mesa: fix interpretation of glClearBuffer(drawbuffer)
- dri_util: Don't assume __DRIcontext->driverPrivate is a gl_context
- mesa: Fix error code generation in glBeginConditionalRender()
- Use line number information from entire function expression
- i965: Fix 3DSTATE_PUSH_CONSTANT_ALLOC_PS packet creation
- i915: Add support for gl_FragData[0] reads.
- st/mesa: use pipe_sampler_view_release() (fixes segfault)
- llvmpipe: use pipe_sampler_view_release() to avoid segfault
- clover: Remove unused variable
- pipe_loader/sw: close dev->lib when initialization fails
- radeon/compute: Stop leaking LLVMContexts in radeon_llvm_parse_bitcode
- r600/compute: Free compiled kernels when deleting compute state
- r600/compute: Use the correct FREE macro when deleting compute state
- radeon/llvm: Free target data at end of optimization
- st/vdpau: Destroy context when initialization fails
- r600/pipe: Stop leaking context->start_compute_cs_cmd.buf on EG/CM
- r600g: fix SUMO2 pci id
- Revert "mesa: Remove GLXContextID typedef from glx.h."
- glcpp: error on multiple #else/#elif directives
- st/mesa: fix glClear with multiple colorbuffers and different formats
- i965/gen6: Fix HiZ hang in WebGL Google Maps
- glsl: Teach ir_variable_refcount about ir_loop::counter variables.
- glsl: Fix inconsistent assumptions about ir_loop::counter.
- nv50: fix a small leak on context destroy

* Fri Dec 13 2013 tv <tv> 10.0.1-1.mga4.tainted
+ Revision: 556550
- new release

* Fri Dec 06 2013 tmb <tmb> 10.0.0-2.mga4.tainted
+ Revision: 555503
- push to /release

* Mon Dec 02 2013 tv <tv> 10.0.0-1.mga4.tainted
+ Revision: 554635
- new release

* Thu Nov 28 2013 tv <tv> 9.2.4-1.mga4.tainted
+ Revision: 553821
- new release

* Wed Nov 13 2013 tv <tv> 9.2.3-1.mga4.tainted
+ Revision: 551068
- new release

* Wed Nov 06 2013 tmb <tmb> 9.2.2-1.mga4.tainted
+ Revision: 549804
- update to 9.2.2 (maintenance release)

* Mon Oct 21 2013 umeabot <umeabot> 9.2.1-2.mga4.tainted
+ Revision: 539306
- Mageia 4 Mass Rebuild

* Sat Oct 05 2013 tmb <tmb> 9.2.1-1.mga4.tainted
+ Revision: 491931
- disable fix for subdir-objects warning as it breaks the build
+ tv <tv>
- try to fix build
--enable-gallium-g3dvl no more exists
- new release
- new release
- 9.2 RC2
+ fwang <fwang>
- drop warning in automake 1.14

* Thu Aug 22 2013 tv <tv> 9.2.0-0.rc1.1.mga4.tainted
+ Revision: 469191
- libllvmradeon is no more
- adjust file list
- adjust file list
- BR libelf-devel
- 9.2.0 RC1

* Sun Aug 11 2013 tmb <tmb> 9.1.6-2.mga4.tainted
+ Revision: 465405
- rebuild with new gcc

* Sat Aug 03 2013 fwang <fwang> 9.1.6-1.mga4.tainted
+ Revision: 462788
- new version 9.1.6

* Thu Jul 18 2013 fwang <fwang> 9.1.5-1.mga4.tainted
+ Revision: 455491
- new version 9.1.5

* Tue Jul 02 2013 fwang <fwang> 9.1.4-1.mga4.tainted
+ Revision: 449605
- new version 9.1.4

* Thu May 23 2013 fwang <fwang> 9.1.3-1.mga4.tainted
+ Revision: 424801
- new verison 9.1.3

* Wed May 01 2013 tv <tv> 9.1.2-1.mga3.tainted
+ Revision: 411744
- new release

* Sat Apr 27 2013 tmb <tmb> 9.1.1-2.mga3.tainted
+ Revision: 411326
- radeonsi: add new SI pci ids
- r600g: add new richland pci ids

* Wed Mar 20 2013 tv <tv> 9.1.1-1.mga3.tainted
+ Revision: 404192
- do not hardcode version in file list
- new release
+ fwang <fwang>
- update llvmradeonmajor

* Sat Feb 23 2013 tmb <tmb> 9.1.0-1.mga3.tainted
+ Revision: 400079
- update to 9.1 final

* Thu Feb 14 2013 cjw <cjw> 9.1.0-0.git20130213.1.mga3.tainted
+ Revision: 398478
- new snapshot

* Sat Feb 02 2013 cjw <cjw> 9.1.0-0.git20130201.1.mga3.tainted
+ Revision: 394036
- new snapshot from 9.1 branch (20130201)

* Wed Jan 16 2013 cjw <cjw> 9.1.0-0.git20130114.1.mga3.tainted
+ Revision: 388874
- new git snapshot (20130114)
- disable patch901, apparently not needed anymore

* Sat Jan 12 2013 umeabot <umeabot> 9.1.0-0.git20121228.3.mga3.tainted
+ Revision: 359973
- Mass Rebuild - https://wiki.mageia.org/en/Feature:Mageia3MassRebuild

* Sat Dec 29 2012 cjw <cjw> 9.1.0-0.git20121228.2.mga3.tainted
+ Revision: 336233
- build osmesa separately again since this is still needed, with a workaround for the build problem

* Sat Dec 29 2012 cjw <cjw> 9.1.0-0.git20121228.1.mga3.tainted
+ Revision: 336175
- 9.1 snapsnot from 20121228
- disable separate osmesa build since it fails to build, let's hope it is not needed anymore
- move autoreconf to build section where it belongs
- drop unrecognized configure options --enable-shared-dricore and --disable-va
+ tv <tv>
- rebuild for new llvm

* Tue Dec 18 2012 anssi <anssi> 9.0.1-2.mga3.tainted
+ Revision: 332631
- build OSMesa separately without shared-glapi instead of patching
  undefined symbols, since that was not enough to get a properly working
  OSMesa

* Sat Nov 17 2012 fwang <fwang> 9.0.1-1.mga3.tainted
+ Revision: 319028
- new version 9.0.1

* Wed Oct 17 2012 fwang <fwang> 9.0.0-2.mga3.tainted
+ Revision: 307643
- add krh patch to build with wayland 0.99
- rebuild for new wayland

* Tue Oct 09 2012 tv <tv> 9.0.0-1.mga3.tainted
+ Revision: 303797
- fix blino packaging so that stable is newer than git snapshot
- new release
- drop patch 101, 201, 202 (merged)

* Thu Oct 04 2012 rtp <rtp> 9.0.0-0.git20120929.3.mga3.tainted
+ Revision: 302732
- enable some dri drivers on arm to workaround ftbfs.
+ tv <tv>
- temporary obsolete older cauldron vdpau-driver-* packages

* Mon Oct 01 2012 tv <tv> 9.0.0-0.git20120929.2.mga3.tainted
+ Revision: 301233
- libify vdpau-driver-* names like GL drivers so that biarch systems can use them too

* Sun Sep 30 2012 blino <blino> 9.0.0-0.git20120929.1.mga3.tainted
+ Revision: 300616
- use System/Libraries group for vdpau drivers
- remove merged hunk in patch1
- update to snapshot of 9.0 branch from 20120929

* Thu Sep 13 2012 tv <tv> 9.0.0-0.git20120907.2.mga3.tainted
+ Revision: 293701
- rebuild for new x11-server

* Fri Sep 07 2012 blino <blino> 9.0.0-0.git20120907.1.mga3.tainted
+ Revision: 289617
- revert osmesa major to 8
- update to snapshot of 9.0 branch from 20120907

* Fri Sep 07 2012 blino <blino> 9.0.0-0.git20120906.1.mga3.nonfree
+ Revision: 289603
- update descriptions to state that mesa supports OpenGL 3.0

* Fri Sep 07 2012 blino <blino> 9.0.0-0.git20120906.1.mga3
+ Revision: 289384
- add second patch for the radeon parallel build
- remove libGLU packages, they have been split upstream
- run autoreconf in prep, not build
- bump osmesa major
- enable again radeonsi now that upstream fixed symbol conflicts with r600g
- rediff undefined symbols patch
- use upstream patch for the parallel radeon build issue
- remove i965 patches from Chromium for issues that have been fixed in 9.0
- update to snapshot of 9.0 branch from 20120906

* Mon Aug 06 2012 blino <blino> 8.1.0-0.git20120802.6.mga3.tainted
+ Revision: 279553
- use the Video group for vdpau drivers (instead of System/Libraries, so
  that they are not listed by urpmi_rpm-find-leaves -g)

* Mon Aug 06 2012 luigiwalser <luigiwalser> 8.1.0-0.git20120802.5.mga3.tainted
+ Revision: 278988
- rebuild for libffi

* Sun Aug 05 2012 blino <blino> 8.1.0-0.git20120802.4.mga3.tainted
+ Revision: 278734
- add another patch from marcheu/ChromiumOS to skip HiZ flush if no drawing has been done
- add patch from marcheu/ChromiumOS to fix clutter apps segfault on i965 (e.g. gnome-control-center, rh #842413)
- use apply_patches macro

* Sun Aug 05 2012 anssi <anssi> 8.1.0-0.git20120802.3.mga3.tainted
+ Revision: 278531
- fix undefined symbols in libOSMesa and libglapi
  (Fix-undefined-symbols-in-libOSMesa-and-libglapi.patch)

* Sat Aug 04 2012 blino <blino> 8.1.0-0.git20120802.2.mga3.tainted
+ Revision: 278333
- use a better patch to fix llvmpipe link
- disable radeonsi again, egl_gallium fails to link if both r600 and
  radeonsi are enabled (reported as fdo #52167)
- build radeonsi gallium driver: dri and vdpau (requires llvm >= 3.1)

* Fri Aug 03 2012 blino <blino> 8.1.0-0.git20120802.1.mga3.tainted
+ Revision: 278175
- update to snapshot from 20120802
- remove unneeded gl.pc patch now that osmesa.pc is in drivers/osmesa

* Thu Aug 02 2012 anssi <anssi> 8.1.0-0.git20120727.3.mga3.tainted
+ Revision: 278008
- enable libosmesa library for off-screen rendering
- fix parallel make (mesa-radeon-parallel-make.patch)
- fix installation of gl.pc when osmesa is enabled
  (mesa-8.1-fixglpc.patch from Fedora)
- replace deprecated configure parameter
- fix build with current llvm
- own /usr/include/GL

* Thu Aug 02 2012 guillomovitch <guillomovitch> 8.1.0-0.git20120727.2.mga3.tainted
+ Revision: 277908
- rebuild for latest libglew

* Sat Jul 28 2012 blino <blino> 8.1.0-0.git20120727.1.mga3.tainted
+ Revision: 275087
- update to snapshot from 20120727

* Tue Jul 24 2012 blino <blino> 8.1.0-0.git20120724.1.mga3.tainted
+ Revision: 274133
- do not build radeonsi yet, it requires LLVM 3.1
- dri drivers: do not require exact mesa release to avoid conflicts when
  having x86_64 drivers from tainted and i586 drivers from core (#6862)
- the main package can not be noarch
- build radeonsi gallium driver
- update to snapshot from 20120724 (for radeonsi)
- make mesa package noarch since it contains only a config file (possible fix for #6862)

* Mon Jul 23 2012 blino <blino> 8.1.0-0.git20120723.1.mga3.tainted
+ Revision: 273748
- update to snapshot from 20120723

* Sun Jul 22 2012 blino <blino> 8.1.0-0.git20120722.1.mga3.tainted
+ Revision: 273557
- update to snapshot from 20120722

* Thu Jul 19 2012 tmb <tmb> 8.1.0-0.git20120715.2.mga3.tainted
+ Revision: 272377
- rebuild with new glibc

* Mon Jul 16 2012 blino <blino> 8.1.0-0.git20120715.1.mga3
+ Revision: 271501
- dricore is now in libdir, create dricore lib sub-packages
- package new /etc/drirc in the mesa package and make dri drivers require it
- libglsl dynamic lib is gone, now a static lib is used by Mesa build system
- workaround llvmpipe test program link failure by removing --as-needed option for these (reported as fdo #52167)
- remove nouveau backports (already in Mesa 8.1)
- Mesa 8.1 git snapshot from 20120715
- remove obsolete README files from mesa package
- remove unused mesa-driver-install script source
- remove la files since they are not needed by mesa (prepare for mesa 8.1)
- drop unapplied patches
- always run autoreconf, and do not run autoreconf + configure twice when building from git

* Sat Jul 14 2012 blino <blino> 8.0.4-2.mga3
+ Revision: 270761
- build Gallium3D VDPAU plugins (using a package naming scheme similar to vaapi)
- drop obsolete --with-state-trackers=dri configure option,
--with-driver=dri does the same
- drop obsolete --disable-glut option, glut has been removed from upstream sources
- drop obsolete --enable-gallium-nouveau option, replaced by --with-gallium-drivers=nouveau

* Tue Jul 10 2012 anssi <anssi> 8.0.4-1.mga3.tainted
+ Revision: 269296
- new release 8.0.4
- backport nouveau commits from git master to allow build with
  libdrm_nouveau-2 (patches 401..420, plus adaptation patch 421)

* Fri Jun 29 2012 colin <colin> 8.0.3-3.mga3.tainted
+ Revision: 264947
- Rebuild against new libudev major
+ fwang <fwang>
- rebuild for new drm_nouveau

* Wed May 30 2012 tv <tv> 8.0.3-1.mga3
+ Revision: 249561
- new release

* Thu Apr 12 2012 blino <blino> 8.0.2-2.mga2
+ Revision: 230504
- buildrequire llvm-devel since package has been split

* Thu Mar 22 2012 tv <tv> 8.0.2-1.mga2
+ Revision: 225653
- new release

* Thu Mar 01 2012 blino <blino> 8.0.1-3.mga2
+ Revision: 216556
- glesv2 devel package should require egl devel package (for KHR/khrplatform.h include)

* Wed Feb 29 2012 blino <blino> 8.0.1-2.mga2
+ Revision: 215979
- enable gbm (Graphics Buffer Manager) and wayland-egl

* Fri Feb 17 2012 tv <tv> 8.0.1-1.mga2
+ Revision: 210130
- new release

* Sat Feb 11 2012 tv <tv> 8.0-1.mga2.tainted
+ Revision: 207197
- drop unapplied patch 300 that was dropped by FC

* Fri Feb 10 2012 tv <tv> 8.0-1.mga2
+ Revision: 207029
- new release
- mesaGLUT is dead
- libGLw is dead
- drop drivers droped upstream
- drop patch 203 (upstream)

* Thu Dec 22 2011 anssi <anssi> 7.11.2-5.mga2.tainted
+ Revision: 186083
- fix crash with nouveau when opening an image in gwenview 4.7.90
  (nv50-nvc0-use-screen-instead-of-context-for-flush-notifier.patch from
   upstream git master)

* Wed Dec 14 2011 anssi <anssi> 7.11.2-4.mga2.tainted
+ Revision: 181478
- move nouveau drivers from -experimental to main DRI drivers package
  (as per discussion on mageia-dev@)
- drop swrast driver and rename swrastg to replace it (as per discussion
  on mageia-dev@)

* Tue Dec 06 2011 tmb <tmb> 7.11.2-3.mga2.tainted
+ Revision: 177781
- rebuild with gcc-4.6.2

* Mon Dec 05 2011 fwang <fwang> 7.11.2-2.mga2.tainted
+ Revision: 176868
- more patch on llvm 3.0
- more patch on llvm 3.0
- add upstream patch to build with llvm 3.0
- rebuild for new llvm

* Mon Nov 28 2011 tv <tv> 7.11.2-1.mga2.tainted
+ Revision: 173693
- new release
- new release

* Fri Nov 18 2011 fwang <fwang> 7.11.1-1.mga2.tainted
+ Revision: 168659
- new version 7.11.1

* Wed Nov 09 2011 anssi <anssi> 7.11-4.mga2.tainted
+ Revision: 165988
- build with shared dricore to make DRI driver binaries smaller
- re-enable parallel build
- drop old unapplied patch for dricore, implemented upstream

* Tue Oct 04 2011 colin <colin> 7.11-3.mga2.tainted
+ Revision: 151468
- Switch to freeglut now that it's been submitted
+ tv <tv>
- explain why tainted in the SRPM description too
- when tainted, require libtxc-dxtn (mga#1079)
- fix require (libglut-devel => freeglut-devel) (mga#2421)
- enable GL_ARB_texture_float extension in tainted section
- new release
- new prerelease
- new prerelease

* Wed Jul 20 2011 mikala <mikala> 7.11-0.rc2.1.mga2
+ Revision: 127466
- Update tarball to mesa 7.11 rc2
- use pkconfig() for buildrequires
- fix files list

* Tue Jul 12 2011 mikala <mikala> 7.11-0.rc1.1.git20110712.1.mga2
+ Revision: 123310
- Update tarball to mesa 7.11 rc1 (use git tarball)
- enable back mesaglut
- fix file list
- Add udev-devel as BR
- Use git tarball (snapshot of 20110620) (which can really help gnome-shell...)
- Enable gallium drivers (for r300,r600,nouveau,swrast)
- Disable for the moment patch202,300,902,903,904,2004
- Disable build of mesa glut

* Tue Jun 14 2011 dmorgan <dmorgan> 7.10.3-1.mga2
+ Revision: 106004
- Remove P1000 ( Merged upstream )
  Add bison and flex as buildrequires
+ tv <tv>
- new release mgarepo sync -c

* Tue Apr 19 2011 tv <tv> 7.10.2-4.mga1
+ Revision: 88451
- enable EGL since OpenVG support needs EGL
- add OpenVG packages
- remove useless glew BR thus hepling bootstraping
- add OpenGL ES 1 & 2 packages (for chromium and future)
- fix possibility to disable EGL
- make egl-devel requires libegl instead of libgl
- add "generic" provides to libs
- move pkgconfig files to the right places (and add proper Conflicts)
- use %%*majors in %%files definitions
- reorder packages to reduce differences with mdv
- fix a misplaced %%endif
- remove useless tcl & texinfo BR

* Tue Apr 19 2011 tv <tv> 7.10.2-3.mga1
+ Revision: 88346
- drop commented out applying non existing patch 1001
- use %%makeinstall_std
- remove unexistent --with-demos configure option
- remove white spaces at the end of lines
- remove useless exported variables
- there are no .la files, there's nothing to perl -pi -e
- remove useless mkdir
- move some requires from libgl-devel into libglut-devel
- libgl-devel  should require libgl instead of mesa
- don't remove files that don't exist
- add with_mesaglut switch (to help future freeglut conversion)

* Sun Apr 17 2011 tmb <tmb> 7.10.2-2.mga1
+ Revision: 86949
- i965: bump VS thread number to 60 on Sandy Bridge

* Tue Apr 12 2011 tv <tv> 7.10.2-1.mga1
+ Revision: 83583
- new release

* Mon Mar 28 2011 blino <blino> 7.10.1-1.mga1
+ Revision: 78230
- nouveau: fix includes for latest libdrm (from upstream git)
- 7.10.1 (fixes dri2 with r300)

* Sun Jan 23 2011 tmb <tmb> 7.10-1.mga1
+ Revision: 35532
- update to 7.10
- drop P905 (merged)
- add BR: makedepend

* Sat Jan 15 2011 rtp <rtp> 7.9-2.mga1
+ Revision: 18133
- add bootstrap (mesa->glew->mesa)
- fix build with current libdrm
- fix build on arm
+ pterjan <pterjan>
- Drop obsolete scriptlets
- imported package mesa


* Sat Nov 20 2010 Paulo Andrade <pcpa@mandriva.com.br> 7.9-2mdv2011.0
+ Revision: 599080
- Add back patch to correct X Server crash in the salome package
  CCBUG: 59084

* Wed Oct 06 2010 Thierry Vignaud <tv@mandriva.org> 7.9-1mdv2011.0
+ Revision: 583170
- BuildRequires:  libxml2-python
- BuildRequires:  talloc-devel
- new release
- rediff patch 201
- drop merged patches (202 302 904 905)
- mesa-demos was split out
- bump BR on libdrm
  (we should probably have a versioned require on the binary RPM too...)

  + Paulo Ricardo Zanoni <pzanoni@mandriva.com>
    - New version: 7.8.2

* Mon Jun 21 2010 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.8.1-6mdv2010.1
+ Revision: 548491
- Add patch to fix an intel bug (affects glknots and other apps)

* Mon May 17 2010 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.8.1-5mdv2010.1
+ Revision: 545055
- Make invesalius work again (#59269)
  CCBUG 59269

* Tue May 11 2010 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.8.1-4mdv2010.1
+ Revision: 544529
+ rebuild (emptylog)

* Mon May 10 2010 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.8.1-3mdv2010.1
+ Revision: 544433
- Sync with mesa 7.8 branch
- Add patch to work around an Intel crash
  CCBUG: 59084

* Thu Apr 29 2010 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.8.1-2mdv2010.1
+ Revision: 540853
- Add patch that should fix bug #58580

* Mon Apr 05 2010 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.8.1-1mdv2010.1
+ Revision: 531894
- New version: 7.8.1

* Mon Mar 29 2010 Anssi Hannula <anssi@mandriva.org> 7.8-1mdv2010.1
+ Revision: 528705
- new version 7.8
- remove patches applied in upstream release

* Thu Mar 25 2010 Anssi Hannula <anssi@mandriva.org> 7.8-0.rc2.3mdv2010.1
+ Revision: 527416
- really apply patch added in last release

* Wed Mar 24 2010 Anssi Hannula <anssi@mandriva.org> 7.8-0.rc2.2mdv2010.1
+ Revision: 527232
- fix i945 miptree pitch disagreement (fixes mdv #58248, fd.o #26966,
  patch from mesa git master (da011faf48))

* Tue Mar 23 2010 Anssi Hannula <anssi@mandriva.org> 7.8-0.rc2.1mdv2010.1
+ Revision: 526943
- new release candidate 7.8-rc2
- bump buildrequires on libx11-devel, libxext-devel, libxxf86vm-devel,
  libxi-devel (Charles A Edwards)
- bump buildrequires on libdrm (due to nouveau changes)
- fix failing assertion in gl-117 and blender (upstream bug #27034, patch
  from 7.8 git branch)
- fix build of nouveau driver with current libdrm
  (renamed-NVxxTCL_TX_GEN-definitions.patch)

* Thu Mar 18 2010 Anssi Hannula <anssi@mandriva.org> 7.8-0.rc1.1mdv2010.1
+ Revision: 524757
- disable link-shared (libdricore.so) patch for now due to linking
  issues, as per fedora (our problems may be different than theirs,
  though)
- new release candidate 7.8 rc1
- enable nouveau driver, in lib(64)dri-drivers-experimental package
  (fixes #57884)
- remove fedora patches intel-revert-vbl-v1.1.patch,
  mesa-7.1-disable-intel-classic-warn-v1.3.patch, no longer applied by
  fedora
- remove now unneeded DRI-modules-are-not-under-usr-X11R6-anymore.patch
  and Fix-linux-dri-so-it-can-be-used-for-all-archs-thank.patch, both
  are handled by configure script now
- remove old disabled i965 memleak patch
- clean buildroot in the beginning of install stage
- update file lists
- rediff nukeglthread-debug.patch and link-shared.patch

* Tue Mar 16 2010 Anssi Hannula <anssi@mandriva.org> 7.7-5mdv2010.1
+ Revision: 521938
- remove -fno-strict-aliasing from ARCH_FLAGS, added upstream
- remove -fno-tree-vrp from ARCH_FLAGS, fixed in gcc 4.3+
- build with normal optimization (fd.o #10224, i.e. crashes on r200, is
  likely fixed in gcc during the last 3 years)
- remove RPM_OPT_FLAGS from ARCH_FLAGS, as mesa gets the flags from
  CFLAGS now
- split DRI drivers to unversioned lib(64)dri-drivers package
  (experimental ones will go to lib(64)dri-drivers-experimental when such
   drivers will be enabled at a later time)

* Wed Feb 03 2010 Thierry Vignaud <tv@mandriva.org> 7.7-4mdv2010.1
+ Revision: 499974
- move glinfo into glxinfo package too (prevent driconf and the like to require
  the big mesa-demos)

* Sun Jan 24 2010 Anssi Hannula <anssi@mandriva.org> 7.7-3mdv2010.1
+ Revision: 495514
- split glxinfo from mesa-demos into glxinfo package (mesa-demos will
  still pull it in)

* Wed Dec 23 2009 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.7-2mdv2010.1
+ Revision: 481772
+ rebuild (emptylog)

* Tue Dec 22 2009 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.7-1mdv2010.1
+ Revision: 481590
- New version: 7.7

* Wed Dec 16 2009 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.6.1-0.rc4.1mdv2010.1
+ Revision: 479548
- New version: 7.6.1-rc4

* Mon Dec 14 2009 Thierry Vignaud <tv@mandriva.org> 7.6.1-0.rc3.2mdv2010.1
+ Revision: 478455
- enable r600 3D driver too

* Fri Dec 11 2009 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.6.1-0.rc3.1mdv2010.1
+ Revision: 476544
- New version: 7.6.1-rc3
  Removed applied patches

  + Thierry Vignaud <tv@mandriva.org>
    - drop requires from mesa-demos on mesa (empty package but for docs)

* Mon Oct 05 2009 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.5.2-2mdv2010.0
+ Revision: 454106
- Fix problems with r200 and r300 textures.

* Thu Oct 01 2009 Paulo Ricardo Zanoni <pzanoni@mandriva.com> 7.5.2-1mdv2010.0
+ Revision: 452203
- Update to mesa 7.5.2
  Remove patch 0200 (already included in this release)

* Mon Sep 28 2009 Thierry Vignaud <tv@mandriva.org> 7.6-0.1mdv2010.0
+ Revision: 450711
- fix URL

* Sun Sep 27 2009 Anssi Hannula <anssi@mandriva.org> 7.5.1-3mdv2010.0
+ Revision: 450137
- remove duplicate libdricore.so (it already exists in /usr/lib/dri/)

  + Olivier Blin <oblin@mandriva.com>
    - merge mips changes (from Arnaud Patard)

* Thu Sep 17 2009 Colin Guthrie <cguthrie@mandriva.org> 7.5.1-1mdv2010.0
+ Revision: 443963
- Add build fix (cherry-picked from 7.5.2 branch)
- New version: 7.5.1
- Drop upstream patches
- Do not apply master cherry-pick for now until I can research the solution more

  + Guillaume <gc@mandriva.com>
    - bump release
    - add Requires: to workaround buggy pkg-config on mismet private dependencies

* Mon Aug 03 2009 Thierry Vignaud <tv@mandriva.org> 7.5-2mdv2010.0
+ Revision: 408410
- patch 201: upstream fix for black screen (eg: mplayer -vo gl with r300 card)

* Mon Jul 20 2009 Ander Conselvan de Oliveira <ander@mandriva.com> 7.5-1mdv2010.0
+ Revision: 398194
- Update to version 7.5

* Sun Jul 05 2009 Colin Guthrie <cguthrie@mandriva.org> 7.5-0.rc4.2.git20090705.1mdv2010.0
+ Revision: 392644
- Disable parallel make as it causes issues with the shared dricore
- Update to the latest 7.5-branch which fixes issues for intel stuff in rc4.

* Tue Jun 30 2009 Ander Conselvan de Oliveira <ander@mandriva.com> 7.5-0.rc4.1mdv2010.0
+ Revision: 391020
- update to 7.5 RC4
- drop patch 101 (applied upstream)

* Thu Jun 18 2009 Colin Guthrie <cguthrie@mandriva.org> 7.5-0.rc3.3mdv2010.0
+ Revision: 387186
- Add upstream patch for intel memory leak (fdo#20704)
- Remove upstream patch that causes problems with compiz (fdo#20704)

* Mon Jun 08 2009 Colin Guthrie <cguthrie@mandriva.org> 7.5-0.rc3.2mdv2010.0
+ Revision: 383856
- Actually apply the patches I added in the last release (*sigh*)

* Sat Jun 06 2009 Colin Guthrie <cguthrie@mandriva.org> 7.5-0.rc3.1mdv2010.0
+ Revision: 383226
- Add upstream patch for memory leak and proposed patch from intel-gfx list
- Update to RC3

* Mon May 18 2009 Ander Conselvan de Oliveira <ander@mandriva.com> 7.5-0.rc2.1mdv2010.0
+ Revision: 377047
- update to 7.5-rc2
- fix build failure
- update to 7.5-rc1
- drop patch 100 (applied upstream)

* Thu Apr 30 2009 Colin Guthrie <cguthrie@mandriva.org> 7.4.1-1mdv2010.0
+ Revision: 369157
- New version: 7.4.1
- Break egl support (it was disabled anyway)

* Thu Jan 22 2009 Ander Conselvan de Oliveira <ander@mandriva.com> 7.3-1mdv2009.1
+ Revision: 332631
- New version: 7.3

* Fri Jan 16 2009 Ander Conselvan de Oliveira <ander@mandriva.com> 7.3-0.20090116.1mdv2009.1
+ Revision: 330321
- Update to git tag mesa_7_3_rc2

* Sun Jan 11 2009 Christiaan Welvaart <spturtle@mandriva.org> 7.3-0.20081220.2mdv2009.1
+ Revision: 328265
- disable patch301 because it does not support r200

* Tue Dec 30 2008 Colin Guthrie <cguthrie@mandriva.org> 7.3-0.20081220.1mdv2009.1
+ Revision: 321330
- Update to recent git snapshot
- Liberate patches from Fedora
- Convert out patches to git for simpler management
- Drop unneeded patches

* Tue Dec 02 2008 Colin Guthrie <cguthrie@mandriva.org> 7.2-1.20081001.2mdv2009.1
+ Revision: 308947
- Build swrast after the others.. note to self: *test* after making last minute changes
- Build swrast driver explicitly

* Sat Nov 29 2008 Colin Guthrie <cguthrie@mandriva.org> 7.2-1.20081001.1mdv2009.1
+ Revision: 307986
- Update mesa to post-7.2 snapshot (as used by fedora)
- Adapt some patches
- Liberate some fedora patches

* Fri Aug 22 2008 Ander Conselvan de Oliveira <ander@mandriva.com> 7.0.4-1mdv2009.0
+ Revision: 275210
- Disables i915tex dri driver.
- Update to version 7.0.4

  + Thierry Vignaud <tv@mandriva.org>
    - rebuild early 2009.0 package (before pixel changes)

  + Pixel <pixel@mandriva.com>
    - do not call ldconfig in %%post/%%postun, it is now handled by filetriggers

  + Paulo Andrade <pcpa@mandriva.com.br>
    - Make libmesagl owner of  %%{_prefix}/lib/dri on x86_64 (32 bit binaries).

* Fri May 09 2008 Lev Givon <lev@mandriva.org> 7.0.3-2mdv2009.0
+ Revision: 205304
- Add arch-independent provides to library packages.

* Mon Apr 14 2008 Paulo Andrade <pcpa@mandriva.com.br> 7.0.3-1mdv2009.0
+ Revision: 193181
- Update to version 7.0.3.
  Also use gccmakedep instead of makedepend by default, but make it
  configurable. This should remove more than half of the compile warnings,
  due to makedepend not finding several dependencies.

* Mon Mar 10 2008 Paulo Andrade <pcpa@mandriva.com.br> 7.0.2-5mdv2008.1
+ Revision: 183783
- Actually add patches to update documentation, version (7.0.3 rc 2), and
  add two files not available in the "official" 7.0.2 tarball.

* Mon Mar 10 2008 Paulo Andrade <pcpa@mandriva.com.br> 7.0.2-4mdv2008.1
+ Revision: 183716
- Add bug fixes from mesa stable branch. Basically:
    git format-patch mesa_7_0_2..origin/mesa_7_0_branch
  but did not add patches that are not bug fixes, so this should be very
  close to mesa 7.0.3 rc2 without the documentation updates.
  This also has the side effect of "automatically" enabling fortran
  support, that has been asked by a user.

* Tue Jan 15 2008 Paulo Andrade <pcpa@mandriva.com.br> 7.0.2-3mdv2008.1
+ Revision: 153289
- Update BuildRequires.
  Needs to add xgixp and nouveau dri modules.
  Mesa 7.0.3 is expected to be released in 1 to 2 weeks, so no more changes
  to package should be done until the next release.

  + Olivier Blin <oblin@mandriva.com>
    - restore BuildRoot

  + Thierry Vignaud <tv@mandriva.org>
    - kill re-definition of %%buildroot on Pixel's request
    - stop packaging obsolete RELNOTES-[34].*

* Mon Nov 12 2007 Thierry Vignaud <tv@mandriva.org> 7.0.2-2mdv2008.1
+ Revision: 108238
- patch 60: fix build
- new release
- kill patches 44, 46, 47, 100, 101, 102 & 103 (merged upstream)
- make it --short-circuit-able

* Wed Oct 24 2007 Thierry Vignaud <tv@mandriva.org> 7.0.1-12mdv2008.1
+ Revision: 101784
- kill patch 6, now unneeded (added in 6.5-8mdv2007.0, sent by Sebastien savari):
  "static inline functions are broken on some architetures"
- remove patch 9 (fix compiling on x86_64) that I introduced in 6.5-8mdv2007.0
  since it's now unneeded

* Wed Oct 24 2007 Thierry Vignaud <tv@mandriva.org> 7.0.1-11mdv2008.1
+ Revision: 101769
- compile with -O1 since it fixes some freeze on r200 (fd.o bug #10224)
- remove suspicious patch 7 (added by boiko in 6.5-8mdv2007.0, sent by Sebastien
  savari): "in some cases radeon may have 0 bits depth"

* Tue Oct 02 2007 Ademar de Souza Reis Jr <ademar@mandriva.com.br> 7.0.1-10mdv2008.0
+ Revision: 94821
- adding one more patch from claudio as a fix for #34090
  (metisse/neverball crashes on mesa). It's an obvious
  fix for a crash that happens after the previous
  workaround (we now hit what appears to be an
  unrelated bug).

* Tue Oct 02 2007 Ademar de Souza Reis Jr <ademar@mandriva.com.br> 7.0.1-9mdv2008.0
+ Revision: 94680
- adding patch from claudio to workaround bug #34090
  (metisse/neverball crash).  It just avoids the crash until
  we have the real fix, but it's a safe workaround.

* Tue Sep 18 2007 Guillaume Rousse <guillomovitch@mandriva.org> 7.0.1-8mdv2008.0
+ Revision: 89928
- rebuild

* Tue Sep 18 2007 Anssi Hannula <anssi@mandriva.org> 7.0.1-7mdv2008.0
+ Revision: 89724
- rebuild due to package loss

* Tue Sep 04 2007 Olivier Blin <oblin@mandriva.com> 7.0.1-6mdv2008.0
+ Revision: 79234
- mention OpenGL 2.1 support in description and summary (from JLP, #33115)

* Fri Aug 31 2007 Olivier Blin <oblin@mandriva.com> 7.0.1-5mdv2008.0
+ Revision: 76766
- restore gcc options, wrongly modified in commit 69003

* Sun Aug 26 2007 Anssi Hannula <anssi@mandriva.org> 7.0.1-4mdv2008.0
+ Revision: 71693
- remove GL.conf alternative, moved to x11-server-common

* Wed Aug 22 2007 Olivier Blin <oblin@mandriva.com> 7.0.1-3mdv2008.0
+ Revision: 69061
- fix glxinfo segfault with unichrome DRI (reported as upstream bug 12097)
- fix potential NULL dereference in unichrome (freedesktop bug 11879)

* Tue Aug 07 2007 Olivier Blin <oblin@mandriva.com> 7.0.1-2mdv2008.0
+ Revision: 59955
- add support for Intel 965GME/GLE (from upstream git)
- add support for Intel G33, Q33, and Q35 chipsets (from upstream git)
- add support for Intel 945GME (from upstream git)
- build with -fno-tree-vrp to fix r300 misrenderings/freezes
- re-enable parallel build

* Mon Aug 06 2007 Thierry Vignaud <tv@mandriva.org> 7.0.1-1mdv2008.0
+ Revision: 59341
- new release

* Tue Jun 26 2007 Gustavo Pichorim Boiko <boiko@mandriva.com> 7.0-1mdv2008.0
+ Revision: 44280
- New upstream release: 7.0
- Redid patches linux-dri-config and radeon-0depthbits
- Removed patch: google_earth: there is a DRI runtime config option to configure
  the fallback checking.

* Thu Jun 07 2007 Tomasz Pawel Gajc <tpg@mandriva.org> 6.5.3-2mdv2008.0
+ Revision: 36911
- rebuild for expat

* Fri May 04 2007 Colin Guthrie <cguthrie@mandriva.org> 6.5.3-1mdv2008.0
+ Revision: 22173
- Enable i915tex driver (needs new drm/kernel bits to activate)

  + Gustavo Pichorim Boiko <boiko@mandriva.com>
    - New upstream release: 6.5.3

* Tue Apr 17 2007 Gustavo Pichorim Boiko <boiko@mandriva.com> 6.5.2-8mdv2008.0
+ Revision: 13910
- Fix some texture data corruption


* Fri Mar 30 2007 Gustavo Pichorim Boiko <boiko@mandriva.com> 6.5.2-7mdv2007.1
+ Revision: 149942
- Fix moving DRI windows on Matrox cards

* Wed Mar 14 2007 Gustavo Pichorim Boiko <boiko@mandriva.com> 6.5.2-6mdv2007.1
+ Revision: 143503
- check for the context to be valid before using it (probably fixes #29089)

  + Gwenole Beauchesne <gbeauchesne@mandriva.com>
    - ppc64 fixes

* Wed Jan 10 2007 Colin Guthrie <cguthrie@mandriva.org> 6.5.2-4mdv2007.1
+ Revision: 106878
- Fix demo builds on x86_64
- Fix BuildRequires
- Just use a linux-dri target for all architectures. This restores the effects of ARCH_FLAGS so this part has been reverted too.
- Build against new glproto (1.4.8)
- Add -fno-strict-aliasing to build flags as recommended by Mesa developers
- Make Mesa build system understand some extra build flags

* Tue Dec 26 2006 Gustavo Pichorim Boiko <boiko@mandriva.com> 6.5.2-1mdv2007.1
+ Revision: 102110
- It seems parallel building is not as good as I thought it was
- new upstream version (6.5.2)
- Re-added a patch removing an incomplete extension
- new upstream version: 6.5.1
  Highlights of this version:
   * Intel i965 support
   * New OpenGL extensions added
   * New demo program: engine
   * Many fixes and improvements in drivers

* Sun Sep 17 2006 Olivier Blin <oblin@mandriva.com> 6.5-17mdv2007.0
+ Revision: 61745
- fix crash with null context (#25555)

  + Gwenole Beauchesne <gbeauchesne@mandriva.com>
    - use update-alternatives

* Wed Sep 13 2006 Olivier Blin <oblin@mandriva.com> 6.5-15mdv2007.0
+ Revision: 60951
- copy libGL files in _libdir/mesa instead of hardlink them (anyway, the stripping broke the hardlink), or else a rpm weirdness prevents libGL.so.1 from being there (#25553)

* Fri Sep 08 2006 Olivier Blin <oblin@mandriva.com> 6.5-14mdv2007.0
+ Revision: 60557
- hardlink libGL files in _libdir/mesa to prevent proprietary driver installers from removing them

* Tue Sep 05 2006 Gustavo Pichorim Boiko <boiko@mandriva.com> 6.5-13mdv2007.0
+ Revision: 59842
- Add support for DRI on Intel i965

* Thu Aug 31 2006 Gustavo Pichorim Boiko <boiko@mandriva.com> 6.5-12mdv2007.0
+ Revision: 58895
- enable tdfx for x86 and x86_64

* Fri Aug 25 2006 Olivier Blin <oblin@mandriva.com> 6.5-11mdv2007.0
+ Revision: 57986
- ensure all GART allocations are freed on context destruction (r300)
- rediff Patch44, and really apply it (GL may finally work on r300 PCIE)

* Sat Aug 19 2006 Olivier Blin <oblin@mandriva.com> 6.5-10mdv2007.0
+ Revision: 56829
- mesa-6.5-10mdv2007.0
- fix r300 on PCIE (glxinfo now works, but GL apps still make the X server crash)

* Sat Aug 12 2006 Olivier Blin <oblin@mandriva.com> 6.5-9mdv2007.0
+ Revision: 55526
- make sure mesa-source-file-generator is executable
- make sure mesa-driver-install can be executed
- bump release
- add missing glxext.c bits in old mesa-6.5-tfp-fbconfig-attribs.patch
- rename last mesa-6.5-texture-from-pixmap-fixes.patch bits as mesa-6.5-no-ARB_render_texture.patch
- move texture_from_pixmap tokens patch in mesa-6.5-texture_from_pixmap_tokens.patch
- move tfp_entrypoints patch in mesa-6.5-tfp_entrypoints.patch (and use upstream CVS diff)
- Add attrib_list to glXBindTexImageEXT
- remove attribList from glXBindTexImageEXT prototype, it breaks the extension because of incomplete implementation

* Fri Aug 04 2006 Gustavo Pichorim Boiko <boiko@mandriva.com> 6.5-8mdv2007.0
+ Revision: 48074
- remove remaining menu references
- Removed old menu entries from mesa demos.
- disable some checks making Google Earth work on r300 cards (#23854)
- Fixed a typo in previous commit
- There is no i830
- Added provides for the old mesa glu and glut devel packages (#22551)
- Many fixes for linux-dri (thanks Christiaan Welvaart)
- Enable r300 installation
- Added a more recente texture-from-pixmap-fixes patch
- Added two more patches fixing the tfp extension
- rebuild to fix compilation on x86_64
- compile mesa xdemos (glxinfo and glxgears are back).
- put a brief comment for the patches
- Adding some patches (thanks S?\195?\131?\194?\169bastien savari):
    * Fix the texture from pixmap extension implementation
    * Enable r300 support (it is experimental yet)
    * static inline functions are broken on some architetures
    * no exec stack for ELF
    * in some cases radeon may have 0 bits depth
- Added initial EGL support to the package (I left it disabled for now as it
  seems some header files are still missing)
- new upstream release: 6.5
- added a patch fixing the path where dri modules are in.
- moved old changelog to ../misc
- fixed some dependencies
- adding an updated mesa prepared to be used by the X.org 7.0

  + Per Øyvind Karlsen <pkarlsen@mandriva.com>
    - include %%{_includedir}/GL/svgamesa.h on %%{sunsparc}
    - add list of dri drivers to build on sparc
      indent
      fix path to demos in menu item

  + Thierry Vignaud <tvignaud@mandriva.com>
    - fix build (parallel build is broken)
    - better description of GLw too
    - better description of GLU and GLUT

  + Christiaan Welvaart <spturtle@mandriva.org>
    - fix build for ppc: do not build sis by using linux-dri-ppc make target
    - Fix build on ppc by adding svgamesa.h to filelist

  + Helio Chissini de Castro <helio@mandriva.com>
    - Added compile fix patch for x86_64 by Thierry Vignaud

  + Andreas Hasenack <andreas@mandriva.com>
    - renamed mdv to packages because mdv is too generic and it's hosting only packages anyway

* Wed Oct 05 2005 Gwenole Beauchesne <gbeauchesne@mandriva.com> 5.0.2-12mdk
- merge ppc64 build fixes from older branch

* Wed Aug 17 2005 Gwenole Beauchesne <gbeauchesne@mandriva.com> 5.0.2-11mdk
- adapt rpath patch for libtool 1.5.18

* Fri Aug 05 2005 Gwenole Beauchesne <gbeauchesne@mandriva.com> 5.0.2-10mdk
- gcc4 fixes
- remove obsolete libGLU hack

* Thu Feb 10 2005 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 5.0.2-9mdk
- libtool fixes

* Sat Sep 18 2004 Gwenole Beauchesne <gbeauchesne@mandrakesoft.com> 5.0.2-8mdk
- use libtool 1.4 for now

* Sat Aug 21 2004 Christiaan Welvaart <cjw@daneel.dyndns.org> 5.0.2-7mdk
- add BuildRequires: automake1.4
- remove packager tag

* Tue Aug 17 2004 Laurent MONTEL <lmontel@mandrakesoft.com> 5.0.2-6mdk
- Rebuild with new menu

* Fri Jun 11 2004 Laurent MONTEL <lmontel@mandrakesoft.com> 5.0.2-5mdk
- Rebuild
- Reapply patch1

* Fri Jun 04 2004 Laurent Montel <lmontel@mandrakesoft.com> 5.0.2-4mdk
- Rebuild again new gcc

* Fri Jun 04 2004 Laurent Montel <lmontel@mandrakesoft.com> 5.0.2-3mdk
- Fix build against gcc 3.4
- Add patch to nuke unpackaged files (patch from Gb)

