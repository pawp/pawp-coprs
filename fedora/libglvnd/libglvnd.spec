%global __provides_exclude_from %{_libdir}/%{name}
%global __requires_exclude_from %{_libdir}/%{name}

%define _disable_ld_as_needed 1
%global optflags %{optflags} -O3 -Wstrict-aliasing=0
%global ldflags %{ldflags} -fuse-ld=gold

%define major 0
%define libgldispatch %mklibname gldispatch %{major}
%define libopengl %mklibname opengl %{major}
%define devname %mklibname glvnd -d

%define libEGL %mklibname EGL 1
%define libGLdispatch %mklibname GLdispatch 0
%define libGLESv1 %mklibname GLESv1_CM 1
%define libGLESv2 %mklibname GLESv2 2
%define libGL %mklibname GL 1
%define libGLX %mklibname GLX 0
%define libOpenGL %mklibname OpenGL 0

Summary:	The GL Vendor-Neutral Dispatch library
Name:		libglvnd
Version:	1.1.1
Release:	2
License:	MIT
Group:		System/Libraries
Url:		https://github.com/NVIDIA/libglvnd
Source0:	https://github.com/NVIDIA/libglvnd/releases/download/v%{version}/%{name}-%{version}.tar.gz
BuildRequires:	python-libxml2
BuildRequires:	pkgconfig(glproto)
BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(xext)

%description
libglvnd is an implementation of the vendor-neutral dispatch layer for
arbitrating OpenGL API calls between multiple vendors on a per-screen basis.

%files
%doc README.md
%dir %{_sysconfdir}/glvnd
%dir %{_datadir}/glvnd
%dir %{_sysconfdir}/glvnd/egl_vendor.d
%dir %{_datadir}/glvnd/egl_vendor.d
%dir %{_sysconfdir}/egl/egl_external_platform.d/
%dir %{_datadir}/egl/egl_external_platform.d/

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} =  %{version}-%{release}
Requires:       %{name}-opengl%{?_isa} =  %{version}-%{release}
Requires:       %{name}-gles%{?_isa} =  %{version}-%{release}
Requires:       %{name}-glx%{?_isa} =  %{version}-%{release}
Requires:       %{name}-egl%{?_isa} =  %{version}-%{release}
Requires:       %{name}-core-devel%{?_isa} =  %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package        core-devel
Summary:        Core development files for %{name}

%description core-devel
The %{name}-core-devel package is a bootstrap trick for Mesa, which wants
to build against the %{name} headers but does not link against any of
its libraries (and, initially, has file conflicts with them). If you are
not Mesa you almost certainly want %{name}-devel instead.


%package        opengl
Summary:        OpenGL support for libglvnd
Requires:       %{name}%{?_isa} =  %{version}-%{release}

%description    opengl
libOpenGL is the common dispatch interface for the workstation OpenGL API.


%package        gles
Summary:        GLES support for libglvnd
Requires:       %{name}%{?_isa} =  %{version}-%{release}
%{!?_without_mesa_glvnd_default:
# mesa is the default EGL implementation provider
Requires:       mesa-libEGL%{?_isa} >= 13.0.4-1
Provides:       libGLES
Provides:       libGLES%{?_isa}
}

%description    gles
libGLESv[12] are the common dispatch interface for the GLES API.


%package        egl
Summary:        EGL support for libglvnd
Requires:       %{name}%{?_isa} =  %{version}-%{release}
%{!?_without_mesa_glvnd_default:
# mesa is the default EGL implementation provider
Requires:       mesa-libEGL%{?_isa} >= 13.0.4-1
Provides:       libEGL
Provides:       libEGL%{?_isa}
}

%description    egl
libEGL are the common dispatch interface for the EGL API.


%package        glx
Summary:        GLX support for libglvnd
Requires:       %{name}%{?_isa} =  %{version}-%{release}
%{!?_without_mesa_glvnd_default:
# mesa is the default GL implementation provider
Requires:       mesa-libGL%{?_isa} >= 13.0.4-1
Provides:       libGL
Provides:       libGL%{?_isa}
}

%description    glx
libGL and libGLX are the common dispatch interface for the GLX API.

%prep
%autosetup -p1

%build
autoreconf -vif
%configure \
	--disable-static \
	--enable-asm \
	--enable-tls

%make_build

%install
%make_install

# Create directory layout
mkdir -p %{buildroot}%{_sysconfdir}/glvnd/egl_vendor.d
mkdir -p %{buildroot}%{_datadir}/glvnd/egl_vendor.d
mkdir -p %{buildroot}%{_sysconfdir}/egl/egl_external_platform.d
mkdir -p %{buildroot}%{_datadir}/egl/egl_external_platform.d

